# vue3-ts-pro

vue3 + ts + ant-design-vue 搭建的后台管理系统。

参考了市面上其他优秀的 vue3 开源项目，如[vben](https://github.com/vbenjs/vue-vben-admin)、[vue3-antd-admin](https://github.com/buqiyuan/vue3-antd-admin)，并依据cbim业务做了一些修改和扩展。

## 技术栈的选择

antdv： tsx 编写，可在此基础上开发更多自定义组件（纯属个人观点，v3比v2好用：取消了令人费解的scopeslot和其他slot）

## 和老项目比有什么新特性或优化？

1. vue2 ==> vue3
2. js ==> ts
3. vuex ==> pinia
4. webpack ==> vite

---

5. 想要写起来更简单的样式？ ==> less + windi css

   ![img](image/README/1647574417274.png)![img](image/README/1647576072725.png)![](image/README/1647576138422.png)
6. 想要更多的 hooks? ==> vueuse + 自定义 hooks
7. 代码格式化不统一，各自提交后找不到 git 里的差异? ==> prettier
8. 部署到其他服务器时不知道版本是否正确? ==> 在 meta 元信息中查看
9. 图标不够用? ==> iconify [+ svg] + antdicon [+ iconfont]
10. 官方时间库轻量化 moment ==> dayjs
11. 稍微来点加密 ==> crypto-js 加密 storage 的内容

## 新业务框架能带来什么？

利用以上的新特性，除了框架本身的不同（语法、库 ......）,还二次封装了很多新组件。让我们看看新组件和之前的有什么不同

需求：实现如下页面功能

![](image/README/1646790862303.png)

vue2项目中：dom+双向绑定

![](image/README/1646876843990.png)

新项目中提供了另一种实现思路

![](image/README/1646878137457.png)

借助vue3 组合式api、响应性api(ref, watchEffect, computed, reactive, watch) 和 antdv，将一个组件拆分成小单元，实现高复用性和灵活性。

![img](image/README/1646879315189.png)+![img](image/README/1646877502467.png)

* 不再需要繁琐的在data中创建变量，然后绑定再赋值
* 打开modal、drawer不用需要绑定visible，控制该字段的值
* 不再需要mixin这种写起来很方便，维护起来很糟糕的东西
* 上面的例子中，可以使用basicTable内置的form，也可以使用basicForm+basicTable
* ......

当然，

1. 不管是hooks还是tsx，当一个项目的拆分的单元越来越多，维护起来也是灾难级别的，所以也不是说单元就越多越好，自己把握就行。
2. 通过hooks函数传递参数，会导致单项数据流不够清晰
3. 组件不可能包含所有的业务情景，遇到复杂的页面，请自行封装业务组件，或使用最基础的template语法

## 准备

- [node](http://nodejs.org/) - 升级到最新版本
- [Vite](https://vitejs.dev/) - 熟悉 vite 特性
- [Vue3](https://v3.vuejs.org/) - 熟悉 Vue 基础语法
- [TypeScript](https://www.typescriptlang.org/) - 熟悉 `TypeScript`基本语法
- [Es6+](http://es6.ruanyifeng.com/) - 熟悉 es6 基本语法
- [Vue-Router-Next](https://next.router.vuejs.org/) - 熟悉 vue-router 基本使用
- [Ant-Design-Vue](https://next.antdv.com/docs/vue/introduce-cn/) - ui 基本使用
- [Mock.js](https://github.com/nuysoft/Mock) - mockjs 基本语法
- [windi css](https://windicss.org/) - 熟悉 windicss
- [Iconify](https://iconify.design/) - 熟悉图标库
- [pinia](https://pinia.vuejs.org/) - 熟悉状态管理
- [vueuse](https://vueuse.org/) - 熟悉组合式 api 工具库

## 安装使用

- 安装推荐的插件
- 安装依赖

```bash
npm install
```

- 初始化项目

```bash
npm run init
```

- 运行

> 此项目配备相应的nodejs后台，admin/admin

```bash
npm run dev
```

## 打包发布

- 打包

1. 修改 `package.json`中的 `version`
2. 测试环境

```bash
npm run build:test
```

3. 生产环境

```bash
npm run build
```

4. 发布记录中同时记录版本号

比如初始为：1.0.0，功能修复或者小修改时为1.0.x；发生重大里程碑变更时为2.0.x

## 项目目录

```
.
|-- build					打包相关
|   |-- config				打包配置文件
|   |-- plugin				vite插件
|-- dist					打包文件夹
|-- mock
|-- public					全局资源文件
|   |-- resource
|       |-- img
|-- src
|   |-- api					http请求
|   |-- assets				静态资源
|   |   |-- icons
|   |   |-- images			图片
|   |   |-- svg				SVG图片
|   |-- basicComp			基本组件，基于antdv封装
|   |   |-- Application		app级别组件
|   |   |-- Basic			基础组件
|   |   |-- Container
|   |   |-- Drawer
|   |   |-- Dropdown
|   |   |-- Form
|   |   |-- Icon
|   |   |-- Loading
|   |   |-- Modal
|   |   |-- Page
|   |   |-- Scrollbar
|   |   |-- SimpleMenu
|   |   |-- Table
|   |   |-- Transition
|   |   |-- Upload
|   |   |-- components.ts			导出所有组件
|   |   |-- hooks.ts				导出所有hooks
|   |   |-- registerGlobComp.ts		注册一些全局组件
|   |   |-- types.ts				导出所有types
|   |-- components					本项目的业务组件
|   |   |-- Application
|   |   |-- TreeSelect
|   |   |-- Upload
|   |-- design						全局的less变量
|   |-- directive 					指令
|   |-- enums 						所有枚举值
|   |-- hooks						钩子函数
|   |   |-- component				组件相关
|   |   |-- core
|   |   |-- event					浏览器事件相关
|   |   |-- setting					项目设置相关
|   |   |-- web						页面相关
|   |-- layouts						基本布局
|   |   |-- default
|   |   |-- iframe
|   |   |-- page
|   |-- router	路由
|   |-- settings	项目设置
|   |   |-- componentSetting.ts		组件相关：设置滚动条，表格参数...
|   |   |-- designSetting.ts		项目颜色主题
|   |   |-- encryptionSetting.ts	设置加密参数、设置storage过期时间
|   |   |-- projectSetting.ts		项目设置
|   |-- store						pinia
|   |-- style						与项目相关样式
|   |-- utils						工具类，<strong>使用频率较高
|   |   |-- auth					获取token,设置/获取权限相关信息
|   |   |-- axios
|   |   |-- cache					storage相关
|   |   |-- event					浏览器相关事件
|   |   |-- helper
|   |   |-- theme					主题相关
|   |   |-- color.ts				颜色
|   |   |-- createAsyncComponent.tsx	异步组件
|   |   |-- dateUtil.ts				时间工具
|   |   |-- domUtils.ts				dom工具
|   |   |-- env.ts					环境文件
|   |   |-- index.ts				公用方法
|   |   |-- is.ts					类型判断
|   |   |-- propTypes.ts
|   |   |-- request.ts				创建axios实例
|   |   |-- uuid.ts
|   |-- views						视图
|-- types							类型定义
```

## 项目开发规范

### 开发新项目

* `git clone` 后修改项目remote地址，删除demo相关的视图和路由，如下所示：

```
src
|-- api
|   |-- demo
|   |   `-- index.ts
|-- router
|   |-- routes
|   |   |-- async-routes.ts			删除部分代码
|   |   `-- modules				保留该文件夹，删除内部ts文件
|   |       |-- demo.ts
|   |       |-- level.ts
|   |       `-- outer.ts
`-- views
    |-- demo
    |   |-- Demo.vue				只保留该文件
    |   |-- drawer
    |   |   |-- Drawer1.vue
    |   |   |-- Drawer2.vue
    |   |   |-- Drawer3.vue
    |   |   |-- Drawer4.vue
    |   |   |-- Drawer5.vue
    |   |   `-- index.vue
    |   |-- echart
    |   |   `-- Bar.vue
    |   |-- form
    |   |   |-- Basic.vue
    |   |   |-- DynamicForm.vue
    |   |   |-- RuleForm.vue
    |   |   |-- basic
    |   |   |   |-- data.ts
    |   |   |   `-- index.vue
    |   |   `-- step
    |   |       |-- Step1.vue
    |   |       |-- Step2.vue
    |   |       |-- Step3.vue
    |   |       |-- component
    |   |       |   `-- PriceInput.vue
    |   |       `-- index.vue
    |   |-- index.vue
    |   |-- modal
    |   |   |-- Modal1.vue
    |   |   |-- Modal2.vue
    |   |   |-- Modal3.vue
    |   |   |-- Modal4.vue
    |   |   `-- index.vue
    |   |-- table
    |       `-- index.vue
```

### 项目设置

- 项目设置请修改 `src/settings/projectSetting.ts`

### src中的目录

* 如果一级菜单内的有很多二级菜单，且二级菜单包含很多自定义业务组件，请新建二级菜单的文件夹，如包含 `index.vue`和 `components文件夹`

![img](image/README/1646880090190.png)  vs ![img](image/README/1646880130516.png)

### ts 相关

- 一些全局的 ts 类型请见 `types/global.d.ts`和 `types/index.d.ts`
- 其他功能性的类型请在 `types/*.d.ts`文件中修改

### 组件使用示例

- 项目在开发或测试模式下会引入组件示例，菜单名为 `Demo`，详见 `src/views/demo`文件夹

### 组件

- 组件共有基础组件和业务组件 2 部分
- 项目的基础组件在 `src/basicComp`文件夹中，别名为 `#`，通过 `import { } from '#/components'`、`#/hooks`、`import type {  } from '#/types'`引用即可
- 基础组件和业务无关，请保持该部分的**通用性**
- 与具体项目相关的业务组件请放到 `src/components`中，**不要**放到 `basicComp`文件夹中

#### 新增组件注意事项

1. 若新增一个可通过 `useXXX`方法使用的组件，**务必**在该组件中新建 `types/**.ts`或 `typing.ts`文件列举所有属性并增加中文注释

#### 在 `useForm`中新增组件

1. 根据组件的**独立性**，在 `src/components`或 `src/basicComp`文件夹中新增组件
2. 新增的自定义组件若要放到 `useForm`中，*暂时*在 `#/Form/src/componentMap.ts`中引入，并增加相应的枚举值，同时请在 demo 中新增相应示例

### mock

- mock 文件夹中的以 `_`开头的文件不会被打包与识别，所以工具类文件请以 `_`开头

### 图标

- svg 雪碧图请放到 `src/assets/icons`文件夹中
- 图标使用 `iconify`、`iconfont`和 `ant-design-icons`

### 主题、颜色、全局变量

- 主题颜色请修改 `build/config/themeConfig.js`的 `primaryColor`
- 全局的 `less`变量请放到 `src/design/var`文件夹中，系统会自动加载
- 全局的 `less`前缀变量请修改 `src/settings/styleSetting.ts`的 `prefix`变量以及 `src\design\var\index.less`的 `@namespace`变量
- `src/design` 定义系统级别的变量并可以全局使用
- 项目相关的 `less`文件请放到 `src/style`文件夹中

### 路由

- 前端和后台都拥有所有路由项，由后台根据权限返回路由(后台返回格式见下方)，前端筛选动态生成，前端排序
- 路由具体配置说明详见 `types/vue-router.d.ts`
- 路由请放到 `src/router/routes/async-routes.ts`文件夹中
- 路由 `name`不能重复
- 除了 `layout`对应的 `path`前需要加 `/`，其余子路由都不要以 `/`开头

### 代码和格式化

- tab缩进 4
- 不要去修改格式化的内容
- 将 `prettier`设置为默认格式化方式
- **文件完成后务必右键格式化！！！**

## 后台的基础接口

1. 后台的 `login`接口只应返回用户 `token`不返回其他信息
2. 后台需提供 `getUserInfo`接口，返回格式如下或其他

```ts
interface GetUserInfoModel {
	userId: string | number;
	// 在mode!=ROUTE_MAPPING时必须！通过权限控制，返回该用户的首页的路由地址
	homePath: string;
	username?: string;
	name: string;
	// 角色信息可根据业务修改
	roleCode: string[];
	roleId: number[];
	roleName: string[];
}
```

3. 后台需提供 `getMenuList`接口，返回格式如下

```ts
interface BackRoute {
	id: number;
	parentId: number;
	name: string;
	title?: string;
	path?: string;
}
export type Array<BackRoute>
```

4. 文件上传是否可采用一个接口，由前端过滤文件类型？
5. 建议图片的预览、显示走 `nginx`代理？

## TODO list

### overview

> _question： 现阶段若有多个项目，那新增的公共组件如何移植到各个项目中去？_

- [ ] 将基础组件库抽离成 `npm`包引入，减少和项目的耦合

### bug

* [X] 当一个父级菜单下有多个子菜单，而角色菜单权限不同时，没有第一个菜单权限时将会出错
* [X] 没有移除storage的方法
* [ ] 多个http请求返回的是同一个实例
* [ ] 页面异常时返回首页出错

### 组件

### router

* [X] ROUTE_MAPPING模式下，将和后端关联的变量改为可配置

### table 组件

- [ ] 重置时同时清空筛选和排序
- [X] 单元格可编辑
- [X] 返回表单中的查询条件

### form 组件

- [X] 结合自定义组件使用
- [ ] 适配多栏form
- [X] 支持重置某一个表单的值
- [X] 增加一个year时间选择框

## 线上环境

[gitee地址](https://gitee.com/sinceseason/vue3-antdv3-ts-pro.git "gitee")

[52 测试环境地址](http://192.168.10.52/v3prj "10.52")

## 测试与 bug

因为这是一个从 0 搭建的项目，测试人员目前只有我自己，如发现重大 bug，请在 gitee 的 issue 中提出来，最好附上复现步骤。

## 浏览器支持

本地开发推荐使用 `Chrome 80+` 浏览器
