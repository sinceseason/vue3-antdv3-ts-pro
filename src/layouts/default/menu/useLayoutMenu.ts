import type { Menu } from '@/router/types';
import { watch, ref } from 'vue';
import { getMenus } from '@/router/menus';
import { usePermissionStore } from '@/store/modules/permission';

export function useSplitMenu() {
	// Menu array
	const menusRef = ref<Menu[]>([]);
	const permissionStore = usePermissionStore();

	// Menu changes
	watch(
		[() => permissionStore.getBackMenuList],
		() => {
			genMenus();
		},
		{
			immediate: true,
		},
	);

	async function genMenus() {
		menusRef.value = await getMenus();
		return;
	}

	return { menusRef };
}
