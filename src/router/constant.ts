export const REDIRECT_NAME = 'Redirect';

export const PARENT_LAYOUT_NAME = 'ParentLayout';

export const PAGE_NOT_FOUND_NAME = 'PageNotFound';

// 404, 403, 500, no-data, network_error
export const EXCEPTION_COMPONENT = () => import('@/views/exception/Exception.vue');

/**
 * @description: default layout
 */
export const LAYOUT = () => import('@/layouts/default/index.vue');

export const RouteView = () => import('@/layouts/page/index.vue');

export const Demo = () => import('@/layouts/page/Empty.vue');

/**
 * @description: parent-layout
 */
export const getParentLayout = (name?: string) => {
	return () =>
		new Promise(resolve => {
			resolve({
				name: name || PARENT_LAYOUT_NAME,
			});
		});
};
