import { getEnvMode, isDevMode, isProdMode } from '@/utils/env';
import { Demo, LAYOUT } from '../constant';
import { transformObjToRoute } from '@/router/helper/routeHelper';
import { AppRouteRecordRaw } from '../types';
import demoRoute from './modules/demo';
import funcRoute from './modules/func';
import levelRoute from './modules/level';
import outerRoute from './modules/outer';

let asyncRoute: AppRouteRecordRaw[] = [
	{
		path: 'home',
		name: 'Home',
		component: () => import('@/views/home/Home.vue'),
		meta: {
			title: '首页',
			icon: 'bx:bx-home',
		},
	},
	{
		path: 'emergency',
		name: 'Emergency',
		component: LAYOUT,
		redirect: '/emergency/person',
		meta: {
			title: '应急基础信息管理',
			icon: 'bx:bx-home',
			hideChildrenInMenu: true,
		},
		children: [
			{
				path: 'person',
				name: 'EmergencyPerson',
				component: Demo,
				meta: {
					title: '应急人员管理',
				},
			},
			{
				path: 'material',
				name: 'EmergencyMaterial',
				component: Demo,
				meta: {
					title: '应急物资管理',
				},
			},
		],
	},
];

if (!isProdMode()) {
	asyncRoute = [...asyncRoute, ...levelRoute, ...outerRoute, ...demoRoute, ...funcRoute];
}

export default asyncRoute;

/**
 * back模式下的一些非鉴权的菜单
 * demo菜单只在开发模式显示
 * 生产模式下的其他菜单可自行添加
 */
let backCustomRoutes: AppRouteRecordRaw[] = [];
if (!isProdMode()) {
	transformObjToRoute(demoRoute);
	backCustomRoutes = [...levelRoute, ...outerRoute, ...demoRoute, ...funcRoute];
}
export { backCustomRoutes };
