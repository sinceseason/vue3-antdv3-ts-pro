import { Demo, LAYOUT } from '@/router/constant';
import { AppRouteRecordRaw } from '@/router/types';

const levelRoute: AppRouteRecordRaw[] = [
	{
		path: '/level',
		name: 'Level',
		component: LAYOUT,
		redirect: '/level/menu1/menu1-1',
		meta: {
			icon: 'carbon:user-role',
			title: '多级菜单测试',
		},
		children: [
			{
				path: 'menu1',
				name: 'Menu1Demo',
				redirect: '/level/menu1/menu1-1/menu1-1-1',
				meta: {
					title: 'Menu1',
				},
				children: [
					{
						path: 'menu1-1',
						name: 'Menu11Demo',
						meta: {
							title: 'Menu1-1',
						},
						children: [
							{
								path: 'menu1-1-1',
								name: 'Menu111Demo',
								component: Demo,
								meta: {
									title: 'Menu111',
								},
							},
						],
					},
					{
						path: 'menu1-2',
						name: 'Menu12Demo',
						component: Demo,
						meta: {
							title: 'Menu1-2',
						},
					},
				],
			},
			{
				path: 'menu2',
				name: 'Menu2Demo',
				component: Demo,
				meta: {
					title: 'Menu2',
				},
			},
		],
	},
];

export default levelRoute;
