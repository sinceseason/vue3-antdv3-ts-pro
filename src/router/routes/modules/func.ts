import { LAYOUT } from '@/router/constant';
import { AppRouteRecordRaw } from '@/router/types';

const funcRoute: AppRouteRecordRaw[] = [
	{
		path: '/func',
		name: 'Function',
		component: LAYOUT,
		redirect: '/func/ws',
		meta: {
			title: '功能',
			icon: 'ri:function-fill',
			ignoreAuth: true,
		},
		children: [
			{
				path: 'ws',
				name: 'FuncWebsocket',
				component: () => import('@/views/func/ws/index.vue'),
				meta: {
					title: 'webscoket测试',
				},
			},
			{
				path: 'excel',
				name: 'FuncExcel',
				redirect: '/feat/excel/customExport',
				component: LAYOUT,
				meta: {
					// icon: 'mdi:microsoft-excel',
					title: 'Excel',
				},

				children: [
					{
						path: 'customExport',
						name: 'CustomExport',
						component: () => import('@/views/func/excel/CustomExport.vue'),
						meta: {
							title: '选择导出格式',
						},
					},
					{
						path: 'jsonExport',
						name: 'JsonExport',
						component: () => import('@/views/func/excel/JsonExport.vue'),
						meta: {
							title: 'JSON数据导出',
						},
					},
					{
						path: 'arrayExport',
						name: 'ArrayExport',
						component: () => import('@/views/func/excel/ArrayExport.vue'),
						meta: {
							title: 'Array数据导出',
						},
					},
					{
						path: 'importExcel',
						name: 'ImportExcel',
						component: () => import('@/views/func/excel/ImportExcel.vue'),
						meta: {
							title: '导入',
						},
					},
				],
			},
		],
	},
];

export default funcRoute;
