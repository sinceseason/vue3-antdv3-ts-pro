import { AppRouteRecordRaw } from '@/router/types';
import { cloneDeep } from 'lodash-es';

const tableRoute: AppRouteRecordRaw[] = [
	{
		path: '/demo/basicTable',
		name: 'BasicTableDemo',
		component: () => import('@/views/demo/table/Basic.vue'),
		meta: {
			title: '基础表格示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/simpleUseTable',
		name: 'SimpleUseTableDemo',
		component: () => import('@/views/demo/table/SimpleUseTable.vue'),
		meta: {
			title: 'usetable基础示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/useTable',
		name: 'UseTableDemo',
		component: () => import('@/views/demo/table/UseTable.vue'),
		meta: {
			title: 'usetable复杂示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},

	{
		path: '/demo/treeTable',
		name: 'TreeTableDemo',
		component: () => import('@/views/demo/table/TreeTable.vue'),
		meta: {
			title: '树形表格示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/treeAndTable',
		name: 'TreeAndTableDemo',
		component: () => import('@/views/demo/table/TreeAndTable.vue'),
		meta: {
			title: '树+表格示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/editCellTable',
		name: 'EditCellTable',
		component: () => import('@/views/demo/table/EditCellTable.vue'),
		meta: {
			title: '可编辑单元格示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/editRowTable',
		name: 'EditRowTable',
		component: () => import('@/views/demo/table/EditRowTable.vue'),
		meta: {
			title: '可编辑行示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
];

const formRoute: AppRouteRecordRaw[] = [
	{
		path: '/demo/basicForm',
		name: 'BasicFromDemo',
		component: () => import('@/views/demo/form/Basic.vue'),
		meta: {
			title: '基础表单示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/ruleForm',
		name: 'RuleFromDemo',
		component: () => import('@/views/demo/form/RuleForm.vue'),
		meta: {
			title: '表单验证示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/ruleCascadeForm',
		name: 'RuleCascadeFromDemo',
		component: () => import('@/views/demo/form/RuleCascadeForm.vue'),
		meta: {
			title: '表单联动校验示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/dynamicForm',
		name: 'DynamicFromDemo',
		component: () => import('@/views/demo/form/DynamicForm.vue'),
		meta: {
			title: '动态表单示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
];

const formPageRoute: AppRouteRecordRaw[] = [
	{
		path: '/demo/basicFormPage',
		name: 'BasicFormPageDemo',
		component: () => import('@/views/demo/form/basic/index.vue'),
		meta: {
			title: '基础表单页面示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/stepFormPage',
		name: 'StepFormPageDemo',
		component: () => import('@/views/demo/form/step/index.vue'),
		meta: {
			title: '分布表单页面示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
];

const othersRoute: AppRouteRecordRaw[] = [
	{
		path: '/demo/upload',
		name: 'UploadDemo',
		component: () => import('@/views/demo/upload/index.vue'),
		meta: {
			title: '文件上传示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/echarts',
		name: 'EchartsDemo',
		component: () => import('@/views/demo/echart/Bar.vue'),
		meta: {
			title: 'echarts图表示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/modal',
		name: 'ModalDemo',
		component: () => import('@/views/demo/modal/index.vue'),
		meta: {
			title: '弹框示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/drawer',
		name: 'DrawerDemo',
		component: () => import('@/views/demo/drawer/index.vue'),
		meta: {
			title: '抽屉示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
	{
		path: '/demo/desc',
		name: 'Description',
		component: () => import('@/views/demo/desc/index.vue'),
		meta: {
			title: '描述列表示例',
			ignoreAuth: true,
			hideMenu: true,
		},
	},
];

const demoRoute: AppRouteRecordRaw[] = [
	{
		path: '/demo',
		name: 'Demo',
		component: () => import('@/views/demo/index.vue'),
		meta: {
			title: 'Demo',
			icon: 'fa-solid:directions',
			ignoreAuth: true,
		},
	},
	...tableRoute,
	...formRoute,
	...formPageRoute,
	...othersRoute,
];

export const DemoRouteMap = new Map<string, AppRouteRecordRaw[]>()
	.set('表格使用示例', cloneDeep(tableRoute))
	.set('表单使用示例', cloneDeep(formRoute))
	.set('表单页面示例', cloneDeep(formPageRoute))
	.set('其他示例', cloneDeep(othersRoute));

export default demoRoute;
