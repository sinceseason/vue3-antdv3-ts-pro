import { LAYOUT } from '@/router/constant';
import { AppRouteRecordRaw } from '@/router/types';

const outerRoute: AppRouteRecordRaw[] = [
	{
		path: '/outer',
		name: 'Outer',
		component: LAYOUT,
		redirect: '/outer/iframe',
		meta: {
			title: '外部页面',
			icon: 'bx:bx-home',
			ignoreAuth: true,
		},
		children: [
			{
				path: 'iframe',
				name: 'OuterIframe',
				meta: {
					title: 'IFrame内嵌页面',
					frameSrc: 'https://next.antdv.com/components/overview',
				},
			},
			{
				path: 'https://v3.cn.vuejs.org/',
				name: 'OuterLink',
				meta: {
					title: '外链',
					isLink: true,
				},
			},
		],
	},
];

export default outerRoute;
