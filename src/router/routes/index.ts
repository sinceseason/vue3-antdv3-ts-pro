import type { AppRouteRecordRaw } from '@/router/types';

import { PAGE_NOT_FOUND_ROUTE, REDIRECT_ROUTE } from '@/router/routes/basic';

import { mainOutRoutes } from './mainOut';
import { PageEnum } from '@/enums/pageEnum';
import { LAYOUT } from '../constant';
import asyncRoute, { backCustomRoutes as backRoutes } from './async-routes';

export const asyncRoutes = asyncRoute;

export const RootRoute: AppRouteRecordRaw = {
	path: '/',
	name: 'Root',
	redirect: PageEnum.BASE_HOME,
	component: LAYOUT,
	meta: {
		title: 'Root',
	},
};

export const LoginRoute: AppRouteRecordRaw = {
	path: PageEnum.BASE_LOGIN,
	name: 'Login',
	component: () => import('@/views/login/Login.vue'),
	meta: {
		title: '登录',
	},
};

export const basicRootRoutes = [REDIRECT_ROUTE, PAGE_NOT_FOUND_ROUTE];

// Basic routing without permission
export const basicRoutes = [LoginRoute, RootRoute, ...mainOutRoutes, ...basicRootRoutes];

export const backCustomRoutes = backRoutes;
