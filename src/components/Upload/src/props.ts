import { uploadFile, uploadImg } from '@/api/sys/upload';
import { PropType } from 'vue';

const basicUploadProps = {
	value: {
		type: Array as PropType<(string | number | Recordable)[]>,
		default: () => [],
	},
	// 文件最大多少MB
	maxSize: {
		type: Number as PropType<number>,
		default: 2,
	},
	// 最大数量的文件，Infinity不限制
	maxNumber: {
		type: Number as PropType<number>,
		default: Infinity,
	},
	// 接口的其他参数
	uploadParams: {
		type: Object as PropType<any>,
		default: {},
	},
	// 是否支持ctrl多选文件
	multiple: {
		type: Boolean as PropType<boolean>,
		default: true,
	},
};

export const pictureCardUploadProps = {
	...basicUploadProps,
	// 文件上传接口
	api: {
		type: Function as PropType<PromiseFn>,
		default: uploadImg,
	},
};

export const simpleUploadProps = {
	...basicUploadProps,
	api: {
		type: Function as PropType<PromiseFn>,
		default: uploadFile,
	},
	// 上传组件显示文案
	title: {
		type: String as PropType<string>,
		default: '上传文件',
	},
	// 限制类型
	accept: {
		type: String as PropType<string>,
		default: null,
	},
	disabled: {
		type: [Boolean, Function] as PropType<boolean | Function>,
		default: false,
	},
};
