import { withInstall } from '@/utils';

import pictureCardUpload from './src/components/PictureCardUpload.vue';
import simpleUpload from './src/components/SimpleUpload.vue';

export const PictureCardUpload = withInstall(pictureCardUpload);
export const SimpleUpload = withInstall(simpleUpload);
