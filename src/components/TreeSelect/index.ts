import { withInstall } from '@/utils';
import deptPersonTreeSelect from './src/DeptPersonTreeSelect.vue';

export const DeptPersonTreeSelect = withInstall(deptPersonTreeSelect);
