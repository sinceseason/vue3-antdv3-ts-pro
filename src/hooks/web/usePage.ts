import type { RouteLocationRaw, Router } from 'vue-router';
import { unref } from 'vue';
import { useRouter } from 'vue-router';

import { PageEnum } from '@/enums/pageEnum';
import { isString } from '@/utils/is';
import { REDIRECT_NAME } from '@/router/constant';
import { useAppStoreOutside } from '@/store/modules/app';
import projectSetting from '@/settings/projectSetting';
import { PermissionModeEnum } from '@/enums/appEnum';
import { useUserStoreOutside } from '@/store/modules/user';

export type RouteLocationRawEx = Omit<RouteLocationRaw, 'path'> & { path: PageEnum };

const appStore = useAppStoreOutside();
const { permissionMode = projectSetting.permissionMode } = appStore.getProjectConfig;

function handleError(e: Error) {
	console.error(e);
}

// page switch
export function useGo(_router?: Router) {
	let router;
	if (!_router) {
		router = useRouter();
	}
	const { push, replace } = _router || router;
	function go(opt: PageEnum | RouteLocationRawEx | string = PageEnum.BASE_HOME, isReplace = false) {
		if (!opt) {
			return;
		}
		if (isString(opt)) {
			if (permissionMode == PermissionModeEnum.ROUTE_MAPPING && opt == PageEnum.BASE_HOME) {
				const userStore = useUserStoreOutside();
				opt = userStore.getHomePath;
			}
			isReplace ? replace(opt).catch(handleError) : push(opt).catch(handleError);
		} else {
			const o = opt as RouteLocationRaw;
			isReplace ? replace(o).catch(handleError) : push(o).catch(handleError);
		}
	}
	return go;
}

/**
 * @description: redo current page
 */
export const useRedo = (_router?: Router) => {
	const { push, currentRoute } = _router || useRouter();
	const { query, params = {}, name, fullPath } = unref(currentRoute.value);
	function redo(): Promise<boolean> {
		return new Promise(resolve => {
			if (name === REDIRECT_NAME) {
				resolve(false);
				return;
			}
			if (name && Object.keys(params).length > 0) {
				params['_redirect_type'] = 'name';
				params['path'] = String(name);
			} else {
				params['_redirect_type'] = 'path';
				params['path'] = fullPath;
			}
			push({ name: REDIRECT_NAME, params, query }).then(() => resolve(true));
		});
	}
	return redo;
};
