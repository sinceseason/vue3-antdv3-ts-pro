import { onUnmounted, getCurrentInstance } from 'vue';
import { createContextMenu, destroyContextMenu } from '#/hooks';

export function useContextMenu(authRemove = true) {
	if (getCurrentInstance() && authRemove) {
		onUnmounted(() => {
			destroyContextMenu();
		});
	}
	return [createContextMenu, destroyContextMenu];
}
