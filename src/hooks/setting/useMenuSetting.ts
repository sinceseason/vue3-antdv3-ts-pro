import type { MenuSetting } from '~/config';

import { computed, unref } from 'vue';

import { useAppStore } from '@/store/modules/app';

import { SIDE_BAR_MINI_WIDTH, SIDE_BAR_SHOW_TIT_MINI_WIDTH } from '@/enums/appEnum';
import { MenuModeEnum, MenuTypeEnum, TriggerEnum } from '@/enums/menuEnum';

// const mixSideHasChildren = ref(false);

export function useMenuSetting() {
	const appStore = useAppStore();

	const getCollapsed = computed(() => appStore.getMenuSetting.collapsed);

	const getMenuType = computed(() => appStore.getMenuSetting.type);

	const getMenuMode = computed(() => appStore.getMenuSetting.mode);

	const getMenuFixed = computed(() => appStore.getMenuSetting.fixed);

	const getMenuWidth = computed(() => appStore.getMenuSetting.menuWidth);

	const getTrigger = computed(() => appStore.getMenuSetting.trigger);

	const getMenuTheme = computed(() => appStore.getMenuSetting.theme);

	const getMenuBgColor = computed(() => appStore.getMenuSetting.bgColor);

	const getCanDrag = computed(() => appStore.getMenuSetting.canDrag);

	const getAccordion = computed(() => appStore.getMenuSetting.accordion);

	const getIsSidebarType = computed(() => unref(getMenuType) === MenuTypeEnum.SIDEBAR);

	const getCollapsedShowTitle = computed(() => appStore.getMenuSetting.collapsedShowTitle);

	const getShowHeaderTrigger = computed(() => {
		return unref(getTrigger) === TriggerEnum.HEADER;
	});

	const getIsMixMode = computed(() => {
		return unref(getMenuMode) === MenuModeEnum.INLINE && unref(getMenuType) === MenuTypeEnum.MIX;
	});

	const getRealWidth = computed(() => {
		return unref(getCollapsed) ? unref(getMiniWidthNumber) : unref(getMenuWidth);
	});

	const getMiniWidthNumber = computed(() => {
		const { collapsedShowTitle } = appStore.getMenuSetting;
		return collapsedShowTitle ? SIDE_BAR_SHOW_TIT_MINI_WIDTH : SIDE_BAR_MINI_WIDTH;
	});

	const getCalcContentWidth = computed(() => {
		const width = unref(getRealWidth);

		return `calc(100% - ${unref(width)}px)`;
	});

	// Set menu configuration
	function setMenuSetting(menuSetting: Partial<MenuSetting>): void {
		appStore.setProjectConfig({ menuSetting });
	}

	function toggleCollapsed() {
		setMenuSetting({
			collapsed: !unref(getCollapsed),
		});
	}
	return {
		setMenuSetting,

		toggleCollapsed,

		getMenuFixed,
		getMenuType,
		getMenuMode,
		getRealWidth,
		getCollapsed,
		getMiniWidthNumber,
		getCalcContentWidth,
		getMenuWidth,
		getTrigger,
		getAccordion,
		getIsSidebarType,
		getShowHeaderTrigger,
		getIsMixMode,
		getMenuTheme,
		getCanDrag,
		getCollapsedShowTitle,
		getMenuBgColor,
		// mixSideHasChildren,
	};
}
