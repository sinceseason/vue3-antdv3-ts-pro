import type { HeaderSetting } from '~/config';

import { computed, unref } from 'vue';

import { useAppStore } from '@/store/modules/app';

import { useMenuSetting } from '@/hooks/setting/useMenuSetting';
import { useRootSetting } from '@/hooks/setting/useRootSetting';
import { useFullContent } from '../web/useFullContent';

export function useHeaderSetting() {
	const { getFullContent } = useFullContent();
	const appStore = useAppStore();

	const getShowFullHeaderRef = computed(() => {
		return unref(getShowMixHeaderRef) && unref(getShowHeader);
	});

	const { getShowHeaderTrigger, getIsSidebarType } = useMenuSetting();
	const getUnFixedAndFull = computed(() => !unref(getFixed) && !unref(getShowHeader));

	const getShowInsetHeaderRef = computed(() => {
		const need = !unref(getFullContent) && unref(getShowHeader);
		return need && !unref(getShowMixHeaderRef);
	});

	const { getShowBreadCrumb, getShowLogo } = useRootSetting();

	const getShowMixHeaderRef = computed(() => !unref(getIsSidebarType) && unref(getShowHeader));

	const getHeaderTheme = computed(() => appStore.getHeaderSetting.theme);

	const getShowHeader = computed(() => appStore.getHeaderSetting.show);

	const getFixed = computed(() => appStore.getHeaderSetting.fixed);

	const getHeaderBgColor = computed(() => appStore.getHeaderSetting.bgColor);

	const getShowSearch = computed(() => appStore.getHeaderSetting.showSearch);

	const getUseLockPage = computed(() => appStore.getHeaderSetting.useLockPage);

	const getShowNotice = computed(() => appStore.getHeaderSetting.showNotice);

	const getShowHeaderLogo = computed(() => {
		return unref(getShowLogo) && !unref(getIsSidebarType);
	});

	const getShowContent = computed(() => {
		return unref(getShowBreadCrumb) || unref(getShowHeaderTrigger);
	});

	// Set header configuration
	function setHeaderSetting(headerSetting: Partial<HeaderSetting>) {
		appStore.setProjectConfig({ headerSetting });
	}
	return {
		setHeaderSetting,

		getShowFullHeaderRef,
		getShowInsetHeaderRef,
		getShowSearch,
		getHeaderTheme,
		getUseLockPage,
		getShowNotice,
		getShowHeaderLogo,
		getShowContent,
		getShowHeader,
		getFixed,
		getShowMixHeaderRef,
		getUnFixedAndFull,
		getHeaderBgColor,
	};
}
