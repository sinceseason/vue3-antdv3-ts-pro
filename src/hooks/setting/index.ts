import type { GlobConfig } from '~/config';

import { getAppEnvConfig } from '@/utils/env';

export const useGlobSetting = (): Readonly<GlobConfig> => {
	const {
		VITE_GLOB_APP_TITLE,
		VITE_GLOB_API_URL,
		VITE_GLOB_APP_SHORT_NAME,
		VITE_GLOB_API_URL_PREFIX,
		VITE_GLOB_UPLOAD_URL,
	} = getAppEnvConfig();

	if (!/[a-zA-Z0-9\_]*/.test(VITE_GLOB_APP_SHORT_NAME)) {
		console.warn(`VITE_GLOB_APP_SHORT_NAME 只能由字母和数字组成，请修改.`);
	}

	// Take global configuration
	const glob: Readonly<GlobConfig> = {
		title: VITE_GLOB_APP_TITLE,
		apiUrl: VITE_GLOB_API_URL,
		shortName: VITE_GLOB_APP_SHORT_NAME,
		urlPrefix: VITE_GLOB_API_URL_PREFIX,
		uploadUrl: VITE_GLOB_UPLOAD_URL,
	};
	return glob as Readonly<GlobConfig>;
};
