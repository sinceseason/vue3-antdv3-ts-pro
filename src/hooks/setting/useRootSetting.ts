import type { ProjectConfig } from '~/config';

import { computed } from 'vue';

import { useAppStore, useAppStoreOutside } from '@/store/modules/app';
import { ContentEnum } from '@/enums/appEnum';
import { MenuTypeEnum } from '@/enums/menuEnum';

type RootSetting = Omit<ProjectConfig, 'locale' | 'headerSetting' | 'menuSetting' | 'multiTabsSetting'>;

export function useRootSetting(isOut = false) {
	const appStore = isOut ? useAppStoreOutside() : useAppStore();

	const getPageLoading = computed(() => appStore.getPageLoading);

	const getOpenKeepAlive = computed(() => appStore.getProjectConfig.openKeepAlive);

	const getSettingButtonPosition = computed(() => appStore.getProjectConfig.settingButtonPosition);

	const getCanEmbedIFramePage = computed(() => appStore.getProjectConfig.canEmbedIFramePage);

	const getPermissionMode = computed(() => appStore.getProjectConfig.permissionMode);

	const getShowLogo = computed(() => appStore.getProjectConfig.showLogo);

	const getLogoAsWideAsMenu = computed(
		() => appStore.getMenuSetting.type === MenuTypeEnum.MIX && appStore.getProjectConfig.logoAsWideAsMenu,
	);

	const getContentMode = computed(() => appStore.getProjectConfig.contentMode);

	const getUseOpenBackTop = computed(() => appStore.getProjectConfig.useOpenBackTop);

	const getShowSettingButton = computed(() => appStore.getProjectConfig.showSettingButton);

	const getShowFooter = computed(() => appStore.getProjectConfig.showFooter);

	const getShowBreadCrumb = computed(() => appStore.getProjectConfig.showBreadCrumb);

	const getThemeColor = computed(() => appStore.getProjectConfig.themeColor);

	const getShowBreadCrumbIcon = computed(() => appStore.getProjectConfig.showBreadCrumbIcon);

	const getLockTime = computed(() => appStore.getProjectConfig.lockTime);

	const getLayoutContentMode = computed(() => ContentEnum.FULL);

	function setRootSetting(setting: Partial<RootSetting>) {
		appStore.setProjectConfig(setting);
	}

	return {
		setRootSetting,

		getSettingButtonPosition,
		getLayoutContentMode,
		getPageLoading,
		getOpenKeepAlive,
		getCanEmbedIFramePage,
		getPermissionMode,
		getShowLogo,
		getLogoAsWideAsMenu,
		getShowBreadCrumb,
		getShowBreadCrumbIcon,
		getUseOpenBackTop,
		getShowSettingButton,
		getShowFooter,
		getContentMode,
		getLockTime,
		getThemeColor,
	};
}
