import './style/index.less';
import 'ant-design-vue/dist/antd.less';
import 'virtual:windi-base.css';
import 'virtual:windi-components.css';
import 'virtual:windi-utilities.css';
import { createApp } from 'vue';
import App from './App.vue';
import { registerGlobComp } from './basicComp/registerGlobComp';
import { setupGlobDirectives } from './directive';
import { router, setupRouter } from './router';
import { setupStore } from './store';

const app = createApp(App);

setupStore(app);
registerGlobComp(app);
setupRouter(app);
setupGlobDirectives(app);

router.isReady().then(() => {
	app.mount('#app');
});
