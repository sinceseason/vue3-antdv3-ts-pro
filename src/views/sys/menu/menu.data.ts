import { BasicColumn, FormSchema } from '#/types';
import { h, ref } from 'vue';
import { Tag } from 'ant-design-vue';
import { Icon } from '#/components';
import { getBackMenuLazyTree } from '@/api/sys/menu';

export const columns: BasicColumn[] = [
	{
		title: '菜单名称',
		dataIndex: 'title',
		align: 'left',
	},
	{
		title: '图标',
		width: 80,
		dataIndex: 'icon',
		customRender: ({ record }) => {
			const icon = (record as Recordable)?.icon;
			return icon ? h(Icon, { icon: icon }) : null;
		},
	},
	{
		title: '权限标识',
		dataIndex: 'perms',
	},
	{
		title: '路由路径',
		dataIndex: 'path',
	},
	{
		title: '路由name',
		dataIndex: 'name',
	},
	{
		title: '组件',
		dataIndex: 'component',
		customRender: ({ record }) => {
			const component: string = (record as Recordable)?.component;
			if (!component) return;
			if (component?.toUpperCase() == 'LAYOUT') {
				return component.toUpperCase();
			}
			return `@/views${component}`;
		},
	},
	{
		title: '排序',
		dataIndex: 'orderNo',
		width: 100,
	},
	{
		title: '状态',
		dataIndex: 'status',
		width: 100,
		customRender: ({ record }) => {
			const status = (record as Recordable).status;
			const enable = ~~status === 1;
			const color = enable ? 'green' : 'red';
			const text = enable ? '启用' : '停用';
			return h(Tag, { color: color }, () => text);
		},
	},
	{
		title: '创建时间',
		dataIndex: 'createTime',
	},
];

const isDir = (type: number) => type === 0;
const isMenu = (type: number) => type === 1;
const isButton = (type: number) => type === 2;

export const searchFormSchema: FormSchema[] = [
	{
		field: 'title',
		label: '菜单名称',
		component: 'Input',
		colProps: { span: 8 },
	},
	{
		field: 'status',
		label: '状态',
		component: 'Select',
		componentProps: {
			options: [
				{ label: '停用', value: false },
				{ label: '启用', value: true },
			],
		},
		colProps: { span: 8 },
	},
];

const parentIdParam = ref(0);
export const formSchema: FormSchema[] = [
	{
		field: 'type',
		label: '菜单类型',
		component: 'RadioButtonGroup',
		defaultValue: 1,
		componentProps: {
			options: [
				// { label: '目录', value: 0 },
				{ label: '菜单', value: 1 },
				{ label: '按钮', value: 2 },
			],
		},
		colProps: { lg: 24, md: 24 },
	},
	{
		field: 'name',
		label: '路由name',
		component: 'Input',
		required: true,
		helpMessage: '该值必须唯一,如SystemMenu',
	},
	{
		field: 'title',
		label: '菜单名称',
		component: 'Input',
		required: true,
	},
	{
		field: 'titleAlias',
		label: '菜单别名',
		component: 'Input',
		helpMessage: '用于区分顶部tab名称重复',
	},
	{
		field: 'parentName',
		label: '上级菜单',
		component: 'ApiTreeSelect',
		helpMessage: '默认为一级菜单',
		componentProps: {
			api: getBackMenuLazyTree,
			params: parentIdParam.value,
			immediate: false,
			lazyLoad: true,
			fieldNames: {
				label: 'title',
				value: 'menuId',
			},
			getPopupContainer: () => document.body,
			loadDataParamKey: 'menuId',
		},
	},
	{
		field: 'orderNo',
		label: '排序',
		component: 'InputNumber',
	},
	{
		field: 'icon',
		label: '图标',
		component: 'Input',
		ifShow: ({ values }) => !isButton(values.type),
	},
	{
		field: 'path',
		label: '路由地址',
		component: 'Input',
		required: true,
		helpMessage: ['只有一级菜单以/开头', '如果是外链,即为外链地址'],
		ifShow: ({ values }) => !isButton(values.type),
	},
	{
		field: 'component',
		label: '组件路径',
		component: 'Input',
		helpMessage: ['一级菜单固定为layout(默认值)', '二级子菜单为/a/b/c', '二级父菜单不填'],
		ifShow: ({ values }) => isMenu(values.type),
	},
	{
		field: 'perms',
		label: '权限标识',
		component: 'Input',
		ifShow: ({ values }) => !isDir(values.type),
	},
	{
		field: 'frameSrc',
		label: 'iframe地址',
		component: 'Input',
		ifShow: ({ values }) => isMenu(values.type),
	},
	{
		field: 'status',
		label: '状态',
		component: 'RadioButtonGroup',
		defaultValue: true,
		componentProps: {
			options: [
				{ label: '启用', value: true },
				{ label: '禁用', value: false },
			],
		},
	},
	{
		field: 'isLink',
		label: '是否外链',
		component: 'RadioButtonGroup',
		defaultValue: false,
		componentProps: {
			options: [
				{ label: '否', value: false },
				{ label: '是', value: true },
			],
		},
		ifShow: ({ values }) => !isButton(values.type),
	},
	{
		field: 'ignoreKeepAlive',
		label: '是否忽略缓存',
		component: 'RadioButtonGroup',
		defaultValue: false,
		componentProps: {
			options: [
				{ label: '否', value: false },
				{ label: '是', value: true },
			],
		},
		ifShow: ({ values }) => isMenu(values.type),
	},
	{
		field: 'ignoreAuth',
		label: '是否忽略权限',
		component: 'RadioButtonGroup',
		defaultValue: false,
		componentProps: {
			options: [
				{ label: '否', value: false },
				{ label: '是', value: true },
			],
		},
	},
	{
		field: 'hideMenu',
		label: '是否隐藏菜单',
		component: 'RadioButtonGroup',
		defaultValue: false,
		componentProps: {
			options: [
				{ label: '否', value: false },
				{ label: '是', value: true },
			],
		},
		ifShow: ({ values }) => isMenu(values.type),
	},
	{
		field: 'affix',
		label: '是否固定标签',
		component: 'RadioButtonGroup',
		defaultValue: false,
		componentProps: {
			options: [
				{ label: '否', value: false },
				{ label: '是', value: true },
			],
		},
		ifShow: ({ values }) => isMenu(values.type),
	},
	{
		field: 'hideTab',
		label: '是否隐藏标签',
		component: 'RadioButtonGroup',
		defaultValue: false,
		componentProps: {
			options: [
				{ label: '否', value: false },
				{ label: '是', value: true },
			],
		},
		ifShow: ({ values }) => isMenu(values.type),
	},
	{
		field: 'hideBreadcrumb',
		label: '是否隐藏面包屑',
		component: 'RadioButtonGroup',
		defaultValue: false,
		componentProps: {
			options: [
				{ label: '否', value: false },
				{ label: '是', value: true },
			],
		},
		ifShow: ({ values }) => isMenu(values.type),
	},
	{
		field: 'hideChildrenInMenu',
		label: '是否隐藏子菜单',
		component: 'RadioButtonGroup',
		defaultValue: false,
		componentProps: {
			options: [
				{ label: '否', value: false },
				{ label: '是', value: true },
			],
		},
		ifShow: ({ values }) => isMenu(values.type),
	},
];
