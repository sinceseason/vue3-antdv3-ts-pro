import { defHttp } from '@/utils/request';
import { LoginParams, GetUserInfoModel } from './model/userModel';

import { ErrorMessageMode } from '@/utils/axios/types';

enum Api {
	Login = '/vue3-admin/auth/login',
	// Login = '/smartgr-admin/auth/login',
	Logout = '/logout',
	GetUserInfo = '/vue3-admin/sysuser/info',
	GetPermCode = '/getPermCode',
}

export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
	return defHttp.post<string>(
		{
			url: Api.Login,
			params,
		},
		{
			errorMessageMode: mode,
		},
	);
}

/**
 * @description: getUserInfo
 */
export function getUserInfo() {
	return defHttp.get<GetUserInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}

export function getPermCode() {
	return defHttp.get<string[]>({ url: Api.GetPermCode });
}

export function doLogout() {
	return defHttp.get({ url: Api.Logout });
}
