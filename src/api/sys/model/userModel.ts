/**
 * @description: Login interface parameters
 */
export interface LoginParams {
	username: string;
	password: string;
}

export interface UserInfo {
	userId: string | number;
	name: string;
	username?: string;
	avatar?: string;
	homePath?: string;
	roles?: RoleInfo[];
}

export interface RoleInfo {
	roleId: Nullable<number>;
	roleName: string;
	roleCode: string;
	remark?: string;
	deptId?: string;
	status?: number;
	createTime?: string;
}

/**
 * @description: Get user information return value
 */
export interface GetUserInfoModel {
	userId: string | number;
	// 用户名
	username?: string;
	// 真实名字
	name: string;
	roleList: RoleInfo[];
	homePath?: string;
}
