import type { RouteMeta } from 'vue-router';
export interface RouteItem {
	path: string;
	component: any;
	meta: RouteMeta;
	name?: string;
	alias?: string | string[];
	redirect?: string;
	caseSensitive?: boolean;
	children?: RouteItem[];
}

interface BackRoute {
	id: number;
	parentId: number;
	name: string;
	title: string;
}

/**
 * @description: BACK模式下的返回值格式
 */
export type getBackMenuListResultModel = RouteItem[];

/**
 * @description ROUTE_MAPPING模式下的返回值格式
 */
export type getMappingMenuListResultModel = BackRoute[];
