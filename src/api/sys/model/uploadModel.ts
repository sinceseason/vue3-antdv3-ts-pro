export interface UploadApiResult {
	message: string;
	code: number;
	data: number | string;
}
