import { PermissionModeEnum } from '@/enums/appEnum';
import { useRootSetting } from '@/hooks/setting/useRootSetting';
import { defHttp } from '@/utils/request';
import { unref } from 'vue';

const { getPermissionMode } = useRootSetting(true);
const isRouteMap = unref(getPermissionMode) == PermissionModeEnum.ROUTE_MAPPING;

enum Api {
	List = '/vue3-admin/sysrole/list',
	Role = '/vue3-admin/sysrole',
	RoleMenuTree = '/vue3-admin/sysmenu/tree',
	RoleMenu = '/vue3-admin/sysrolemenu/auth',
	// back模式
	RoleBackMenu = '/vue3-admin/sysrole/back',
	RoleBackMenuAuth = '/vue3-admin/sysrole/back/auth',
}

/**
 * 获取角色列表
 */
export function getRoleList(params = {}) {
	return defHttp.post({ url: Api.List, params });
}

/**
 * 增加或修改角色
 */
export function addOrEditRole(isUpdate: boolean, params = {}) {
	if (isUpdate) {
		return defHttp.put({ url: Api.Role, params });
	}
	return defHttp.post({ url: Api.Role, params });
}

/**
 * 删除角色
 */
export function deleteRole(id: number) {
	if (isRouteMap) {
		return defHttp.delete({ url: Api.Role + '/' + id });
	}
	return defHttp.delete({ url: Api.RoleBackMenu + '/' + id });
}

/**
 * 获取角色所有权限
 */
export function getMenuTree() {
	return defHttp.get({ url: Api.RoleMenuTree });
}

/**
 * 获取角色分配的菜单
 */
export function getMenuByRoleId(roleId: number) {
	if (isRouteMap) {
		return defHttp.get({ url: Api.RoleMenu, params: { roleId } });
	}
	return defHttp.get({ url: Api.RoleBackMenuAuth, params: { roleId } });
}

/**
 * 给角色分配菜单
 */
export function setMenuByRoleId(params = {}) {
	if (isRouteMap) {
		return defHttp.post({ url: Api.RoleMenu, params });
	}
	return defHttp.post({ url: Api.RoleBackMenuAuth, params });
}
