import { UploadFileParams } from '@/utils/axios/types';
import { upload } from '@/utils/request';
import { UploadApiResult } from './model/uploadModel';

enum Api {
	UPLOAD_IMG = '/vue3-upload/img',
	UPLOAD_ACCESSORY = '/vue3-upload/files',
}

export function uploadImg(params: UploadFileParams, onUploadProgress: (progressEvent: ProgressEvent) => void) {
	return upload<UploadApiResult>(
		{
			url: Api.UPLOAD_IMG,
			onUploadProgress,
		},
		params,
	);
}

/**
 * 上传附件
 */
export function uploadFile(params: UploadFileParams, onUploadProgress?: (progressEvent: ProgressEvent) => void) {
	return upload<UploadApiResult>(
		{
			url: Api.UPLOAD_ACCESSORY,
			onUploadProgress,
		},
		params,
	);
}
