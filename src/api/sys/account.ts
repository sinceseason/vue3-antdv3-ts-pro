import { defHttp } from '@/utils/request';

enum Api {
	LIST = '/vue3-admin/sysuser/list',
	QUERY_ALL_ROLE = '/vue3-admin/sysrole',
	USERS = '/vue3-admin/sysuser',
}

/**
 * 获取用户列表
 */
export function getUserList(params = {}) {
	return defHttp.post({ url: Api.LIST, params });
}

/**
 * 查询所有角色
 */
export function getAllRoles() {
	return defHttp.get({ url: Api.QUERY_ALL_ROLE });
}

/**
 * 增加或修改人员
 */
export function addOrEditUser(isUpdate: boolean, params = {}) {
	if (isUpdate) {
		return defHttp.put({ url: Api.USERS, params });
	}
	return defHttp.post({ url: Api.USERS, params });
}

/**
 * 增加或修改人员
 */
export function getUserDetail(id) {
	return defHttp.get({ url: Api.USERS + '/' + id });
}

/**
 * 删除用户
 */
export function deleteUser(userId: number) {
	return defHttp.delete({ url: Api.USERS + '/' + userId });
}
