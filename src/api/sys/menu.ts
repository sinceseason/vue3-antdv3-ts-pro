import { PermissionModeEnum } from '@/enums/appEnum';
import { useRootSetting } from '@/hooks/setting/useRootSetting';
import { defHttp } from '@/utils/request';
import { unref } from 'vue';

const { getPermissionMode } = useRootSetting(true);
const isRouteMap = unref(getPermissionMode) == PermissionModeEnum.ROUTE_MAPPING;

enum Api {
	// routemap 模式
	GetRouteMapMenuList = '/vue3-admin/sysmenu/routeMapMenus/list',
	GetAllRmMenuTree = '/vue3-admin/sysmenu/rmMenus/tree/all',
	// back模式
	GetBackMenu = '/vue3-admin/sysmenu/backMenu',
	GetBackMenuTree = '/vue3-admin/sysmenu/backMenu/auth/tree',
	GetAllBackMenuTree = '/vue3-admin/sysmenu/backMenus/tree/all',
	GetBackMenuLazyTree = '/vue3-admin/sysmenu/backMenus/lazy/tree',
	SaveBackMenu = '/vue3-admin/sysmenu/backMenu/save',
	UpdateBackMenu = '/vue3-admin/sysmenu/backMenu/update',
	DeleteBackMenu = '/vue3-admin/sysmenu/backMenu/delete',
}

/**
 * @description: ROUTE_MAPPING 模式 获取该用户的菜单列表
 */
export function getMenuList<T>() {
	return defHttp.get<T>({ url: Api.GetRouteMapMenuList });
}

/**
 * @description: BACK 模式获取该用户的菜单树
 */
export function getBackMenuList<T>() {
	return defHttp.get<T>({ url: Api.GetBackMenuTree });
}

/**
 * @description: 获取所有菜单树
 */
export function getAllMenuTree(params = {}) {
	if (isRouteMap) {
		return defHttp.post({ url: Api.GetAllRmMenuTree, params });
	}
	return defHttp.post({ url: Api.GetAllBackMenuTree, params });
}

/**
 * @description: BACK 模式根据id获取菜单
 */
export function getBackMenuById<T>(id: number) {
	return defHttp.get<T>({ url: Api.GetBackMenu + '/' + id });
}

/**
 * @description: BACK 模式下懒加载查询菜单树
 */
export function getBackMenuLazyTree<T>(id = 0) {
	return defHttp.get<T>({ url: `${Api.GetBackMenuLazyTree}/${id}` });
}

/**
 * @description: BACK 模式下新增菜单
 */
export function saveBackMenu(menu = {}) {
	return defHttp.post({ url: Api.SaveBackMenu, params: menu });
}
/**
 * @description: BACK 模式下更新菜单
 */
export function updateBackMenu(id: number, menu = {}) {
	return defHttp.put({ url: Api.UpdateBackMenu + '/' + id, params: menu });
}

/**
 * @description: BACK 模式下删除菜单
 */
export function deleteBackMenu(id: number) {
	return defHttp.delete({ url: Api.DeleteBackMenu + '/' + id });
}
