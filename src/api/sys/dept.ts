import { defHttp } from '@/utils/request';

enum Api {
	LIST = '/vue3-admin/sysdept/list',
	DEPT = '/vue3-admin/sysdept',
	DEPT_TREE = '/vue3-admin/sysdept/principal/tree',
}

/**
 * 获取部门列表
 */
export function getDeptList(params = {}) {
	return defHttp.post({ url: Api.LIST, params });
}

/**
 * 增加或修改部门
 */
export function addOrEditDept(isUpdate: boolean, params = {}) {
	if (isUpdate) {
		return defHttp.put({ url: Api.DEPT, params });
	}
	return defHttp.post({ url: Api.DEPT, params });
}

/**
 * 删除部门
 */
export function deleteDept(id: number) {
	return defHttp.delete({ url: Api.DEPT + '/' + id });
}

/**
 * 获取部门树
 */
export function getDeptTree() {
	return defHttp.get({ url: Api.DEPT_TREE });
}
