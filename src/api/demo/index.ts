import { defHttp, upload } from '@/utils/request';
import { UploadFileParams } from '@/utils/axios/types';
import { UploadApiResult } from '../sys/model/uploadModel';

enum Api {
	QUERY_DICT = '/vue3-dict',
	QUERY_LIST = '/vue3-demo/security/list',
	LICENCE_LIST = '/vue3-demo/license/list',
	PRINCIPAL_TREE = '/vue3-admin/sysdept/principal/tree',
	UPLOAD_IMG = '/smartgr-admin/shrqsysimg/filesupload',
	UPLOAD_ACCESSORY = '/vue3-upload/files',
	ADD_QUESTION = '/vue3-demo/security',
	QUERY_USER_LIST_BY_ROLEID = '/smartgr-admin/shrqsysuser/role',
	QUERY_TREE_TABLE = '/table/getTreeTable',
	QUERY_USER_LIST = '/vue3-admin/sysuser/list',
	USERS = '/smartgr-admin/shrqsysuser',
	QUERY_ALL_ROLE = '/smartgr-admin/shrqsysrole',
	QUERY_WH_LIST = '/wh/list',
}

/**
 * 获取安全台账列表
 */
export function getSafeList(
	val = {
		keyWordParam: {},
	},
) {
	return defHttp.post({ url: Api.QUERY_LIST, params: val });
}

/**
 * 获取大类
 */
export function getBigTypes() {
	return defHttp.get({ url: Api.QUERY_DICT, params: { type: 'SecurityBig' } });
}

/**
 * 获取小类
 */
export function getSmallTypes(id) {
	return defHttp.get({
		url: Api.QUERY_DICT,
		params: {
			pid: id,
		},
	});
}

/**
 * 获取证照管理列表
 */
export function getLicenceList(params = {}) {
	return defHttp.post({ url: Api.LICENCE_LIST, params });
}

/**
 * 获取受检单位列表
 */
export function getInspectUnitList(params: Recordable) {
	return defHttp.get({ url: Api.QUERY_DICT, params });
}

/**
 * 获取部门人员树
 */
export function getPrincipaltree() {
	return defHttp.get({ url: Api.PRINCIPAL_TREE });
}

/**
 * 上传图片
 */
export function uploadImg(params: UploadFileParams, onUploadProgress?: (progressEvent: ProgressEvent) => void) {
	return upload<UploadApiResult>(
		{
			url: Api.UPLOAD_IMG,
			onUploadProgress,
		},
		params,
	);
}

/**
 * 上传附件
 */
export function uploadFile(params: UploadFileParams, onUploadProgress?: (progressEvent: ProgressEvent) => void) {
	return upload<UploadApiResult>(
		{
			url: Api.UPLOAD_ACCESSORY,
			onUploadProgress,
		},
		params,
	);
}

/**
 * 创建问题
 */
export function addQuestion(val) {
	return defHttp.post({ url: Api.ADD_QUESTION, params: val });
}

/**
 * 根据roleid查询该角色的所有用户
 */
export function getUserListByRoleId(roleId) {
	return defHttp.get({ url: Api.QUERY_USER_LIST_BY_ROLEID + '/' + roleId });
}

/**
 * 查询人员列表
 */
export function getUserList(params = {}) {
	return defHttp.post({ url: Api.QUERY_USER_LIST, params });
}

/**
 * 获取树形表格
 */
export function getTreeTable() {
	return defHttp.get({ url: Api.QUERY_TREE_TABLE, params: { type: 3 } });
}

/**
 * 增加或修改人员
 */
export function addOrEditUser(isUpdate: boolean, params = {}) {
	if (isUpdate) {
		return defHttp.put({ url: Api.USERS, params });
	}
	return defHttp.post({ url: Api.USERS, params });
}

/**
 * 删除用户
 */
export function deleteUser(userId: number) {
	return defHttp.delete({ url: Api.USERS + '/' + userId });
}

/**
 * @description 查询所有角色
 */
export function getAllRoles() {
	return defHttp.get({ url: Api.QUERY_ALL_ROLE });
}

/**
 * 获取位号列表
 */
export function getWhList() {
	return defHttp.get({ url: Api.QUERY_WH_LIST });
}
