import { defHttp } from '@/utils/request';

enum Api {
	AccountList = '/system/getAccountList',
	IsAccountExist = '/system/accountExist',
	DeptList = '/system/getDeptList',
	setRoleStatus = '/system/setRoleStatus',
	MenuList = '/system/getMenuList',
	RolePageList = '/system/getRoleListByPage',
	GetAllRoleList = '/system/getAllRoleList',
}

export const getMenuList = (params?) => defHttp.get({ url: Api.MenuList, params });
