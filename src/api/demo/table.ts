import { defHttp } from '@/utils/request';
import { DemoParams, DemoListGetResultModel } from './model/tableModel';

enum Api {
	DEMO_LIST = '/table/getDemoList',
	DETAIL_LIST = '/table/getDetailList',
}

/**
 * @description: Get sample list value
 */

export const demoListApi = (params: DemoParams) =>
	defHttp.get<DemoListGetResultModel>({
		url: Api.DEMO_LIST,
		params,
		headers: {
			// @ts-ignore
			ignoreCancelToken: true,
		},
	});

export function getDetailById(params) {
	return defHttp.get({ url: Api.DETAIL_LIST, params });
}
