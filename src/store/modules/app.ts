import { ThemeEnum } from '@/enums/appEnum';
import { APP_DARK_MODE_KEY_, PROJ_CFG_KEY } from '@/enums/cacheEnum';
import { resetRouter } from '@/router';
import { darkMode } from '@/settings/designSetting';
import { deepMerge } from '@/utils';
import { Persistent } from '@/utils/cache/persistent';
import { defineStore } from 'pinia';
import { HeaderSetting, MenuSetting, MultiTabsSetting, ProjectConfig, TransitionSetting } from '~/config';
import { store } from '..';

let timeId: TimeoutHandle;
export const useAppStore = defineStore({
	id: 'app',
	state: () => ({
		darkMode: undefined,
		pageLoading: false,
		projectConfig: Persistent.getLocal(PROJ_CFG_KEY),
	}),
	getters: {
		getPageLoading(): boolean {
			return this.pageLoading;
		},
		getDarkMode(): 'light' | 'dark' | string {
			return this.darkMode || localStorage.getItem(APP_DARK_MODE_KEY_) || darkMode;
		},
		getProjectConfig(): ProjectConfig {
			return this.projectConfig || ({} as ProjectConfig);
		},
		getHeaderSetting(): HeaderSetting {
			return this.getProjectConfig.headerSetting;
		},
		getMenuSetting(): MenuSetting {
			return this.getProjectConfig.menuSetting;
		},
		getTransitionSetting(): TransitionSetting {
			return this.getProjectConfig.transitionSetting;
		},
		getMultiTabsSetting(): MultiTabsSetting {
			return this.getProjectConfig.multiTabsSetting;
		},
	},
	actions: {
		setPageLoading(loading: boolean): void {
			this.pageLoading = loading;
		},
		setDarkMode(mode: ThemeEnum): void {
			(this.darkMode as unknown) = mode;
			localStorage.setItem(APP_DARK_MODE_KEY_, mode);
		},
		setProjectConfig(config: DeepPartial<ProjectConfig>): void {
			this.projectConfig = deepMerge(this.projectConfig || {}, config);
			Persistent.setLocal(PROJ_CFG_KEY, this.projectConfig);
		},
		async resetAllState() {
			resetRouter();
			Persistent.clearAll();
		},
		async setPageLoadingAction(loading: boolean): Promise<void> {
			if (loading) {
				clearTimeout(timeId);
				timeId = setTimeout(() => {
					this.setPageLoading(loading);
				}, 50);
			} else {
				this.setPageLoading(loading);
				clearTimeout(timeId);
			}
		},
	},
});

export function useAppStoreOutside() {
	return useAppStore(store);
}
