import { SessionTimeoutProcessingEnum } from '@/enums/appEnum';
import { useMessage } from '@/hooks/web/useMessage';
import projectSetting from '@/settings/projectSetting';
import { useUserStoreOutside } from '@/store/modules/user';
import { ErrorMessageMode } from './types';

const { createMessage, createErrorModal } = useMessage();
const stp = projectSetting.sessionTimeoutProcessing;

export function checkStatus(status: number, msg: string, errorMessageMode: ErrorMessageMode = 'message'): void {
	const userStore = useUserStoreOutside();
	let errMessage = '';

	switch (status) {
		case 400:
			errMessage = `${msg}`;
			break;
		// 401: Not logged in
		// Jump to the login page if not logged in, and carry the path of the current page
		// Return to the current page after successful login. This step needs to be operated on the login page.
		case 401:
			userStore.setToken(undefined);
			errMessage = msg || '身份验证不通过!';
			if (stp === SessionTimeoutProcessingEnum.PAGE_COVERAGE) {
				userStore.setSessionTimeout(true);
			} else {
				userStore.logout(true);
			}
			break;
		case 403:
			errMessage = '服务器拒绝请求!';
			break;
		case 404:
			errMessage = '未找到该资源!';
			break;
		case 405:
			errMessage = '网络请求错误,请求方法未允许!';
			break;
		case 408:
			errMessage = '网络请求超时!';
			break;
		case 500:
			errMessage = '服务器错误,请联系管理员!';
			break;
		case 501:
			errMessage = '服务器不具备完成请求的功能!';
			break;
		case 502:
			errMessage = '错误网关!';
			break;
		case 503:
			errMessage = '服务不可用，服务器暂时过载或维护!';
			break;
		case 504:
			errMessage = '网关超时!';
			break;
		case 505:
			errMessage = 'http版本不支持该请求!';
			break;
		default:
	}

	if (errMessage) {
		if (errorMessageMode === 'modal') {
			createErrorModal({ title: '错误提示', content: errMessage });
		} else if (errorMessageMode === 'message') {
			createMessage.error({ content: errMessage, key: `global_error_message_status_${status}` });
		}
	}
}
