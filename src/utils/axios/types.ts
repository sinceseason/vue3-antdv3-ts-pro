import type { AxiosRequestConfig, AxiosResponse } from 'axios';

export type ErrorMessageMode = 'none' | 'modal' | 'message' | undefined;

export interface RequestOptions {
	// Splicing request parameters to url
	joinParamsToUrl?: boolean;
	// Format request parameter time
	formatDate?: boolean;
	// 不进行任何处理，直接返回
	// 用于页面代码可能需要直接获取code，data，message这些信息时开启
	isTransformResponse?: boolean;
	// 是否返回原生响应头 比如：需要获取响应头时使用该属性(下载文件流获取文件名)
	isReturnNativeResponse?: boolean;
	// Whether to join url
	joinPrefix?: boolean;
	// Interface address, use the default apiUrl if you leave it blank
	apiUrl?: string;
	// 请求拼接路径
	urlPrefix?: string;
	// Error message prompt type
	errorMessageMode?: ErrorMessageMode;
	// Whether to add a timestamp
	joinTime?: boolean;
	ignoreCancelToken?: boolean;
	// Whether to send token in header
	withToken?: boolean;
	// 区分不同的第三方地址
	apiType?: string;
}

export interface Result<T = any> {
	code: number;
	type?: 'success' | 'error' | 'warning';
	message?: string;
	msg?: string;
	success?: boolean;
	result?: T;
	data: T;
}

export interface UploadFileParams {
	// Other parameters
	data?: Recordable;
	// File parameter interface field name
	name?: string;
	// file name
	file: File | Blob;
	// file name
	filename?: string;
	[key: string]: any;
}

export interface CreateAxiosOptions extends AxiosRequestConfig {
	authenticationScheme?: string;
	transform?: AxiosTransform;
	requestOptions?: RequestOptions;
}

export abstract class AxiosTransform {
	/**
	 * @description: Process configuration before request
	 */
	beforeRequestHook?: (config: AxiosRequestConfig, options: RequestOptions) => AxiosRequestConfig;

	/**
	 * @description: Request successfully processed
	 */
	transformRequestHook?: (res: AxiosResponse<Result>, options: RequestOptions) => any;

	/**
	 * @description 请求失败处理
	 */
	requestCatchHook?: (e: Error, options: RequestOptions) => Promise<any>;

	/**
	 * @description 请求之前的拦截器
	 */
	requestInterceptors?: (config: AxiosRequestConfig, options: CreateAxiosOptions) => AxiosRequestConfig;

	/**
	 * @description 请求之后的拦截器
	 */
	responseInterceptors?: (res: AxiosResponse<any>) => AxiosResponse<any>;

	/**
	 * @description: 请求之前的拦截器错误处理
	 */
	requestInterceptorsCatch?: (error: Error) => void;

	/**
	 * @description: 请求之后的拦截器错误处理
	 */
	responseInterceptorsCatch?: (axiosInstance: AxiosResponse, error: Error) => void;
}
