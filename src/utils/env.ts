import type { GlobEnvConfig } from '~/config';

import pkg from '../../package.json';

export function getCommonStoragePrefix() {
	const { VITE_GLOB_APP_SHORT_NAME } = getAppEnvConfig();
	return `${VITE_GLOB_APP_SHORT_NAME}__${getEnvMode()}`.toUpperCase().replace(/\s/g, '');
}

export function getStorageShortName() {
	return `${getCommonStoragePrefix()}${`__${pkg.version}`}__`.toUpperCase().replace(/\s/g, '');
}

export function getAppEnvConfig() {
	const ENV = import.meta.env as unknown as GlobEnvConfig;

	const {
		VITE_GLOB_APP_TITLE,
		VITE_GLOB_API_URL,
		VITE_GLOB_APP_SHORT_NAME,
		VITE_GLOB_API_URL_PREFIX,
		VITE_GLOB_UPLOAD_URL,
	} = ENV;

	if (!/^[a-zA-Z0-9\_]*$/.test(VITE_GLOB_APP_SHORT_NAME)) {
		console.warn(`VITE_GLOB_APP_SHORT_NAME 只能由英文字母和数字组成，请修改!`);
	}

	return {
		VITE_GLOB_APP_TITLE,
		VITE_GLOB_API_URL,
		VITE_GLOB_APP_SHORT_NAME,
		VITE_GLOB_API_URL_PREFIX,
		VITE_GLOB_UPLOAD_URL,
	};
}

export const devMode = 'development';

export const prodMode = 'production';

export function getEnvMode(): string {
	return import.meta.env.MODE;
}

export function isDevMode(): boolean {
	return import.meta.env.DEV;
}

export function isProdMode(): boolean {
	return import.meta.env.PROD;
}
