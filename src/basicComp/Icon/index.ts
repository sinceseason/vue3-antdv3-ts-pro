import Icon from './src/Icon.vue';
import SvgIcon from './src/SvgIcon.vue';

export { SvgIcon };
export { Icon };
export { Icon as AntdIcon } from './src/AntdIcon';
export { default as IconFont } from './src/IconFont';
export { default as BasicIcon } from './src/BasicIcon.vue';

export default Icon;
