import { createVNode } from 'vue';
import * as Icons from '@ant-design/icons-vue';

type IconProps = {
	// TODO: 将值限制为icons的类型
	icon?: string;
};

const icons = Icons as any;

export const Icon = (props: IconProps) => {
	const { icon } = props;

	if (icon) {
		return createVNode(icons[icon]);
	} else {
		return null;
	}
};
