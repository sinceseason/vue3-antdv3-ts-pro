import { withInstall } from '@/utils';
import basicTree from './src/BasicTree.vue';
import './style';

export const BasicTree = withInstall(basicTree);
export * from './src/types/tree';
