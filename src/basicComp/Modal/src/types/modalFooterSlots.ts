import { Slot, Slots } from 'vue';

export interface ModalFooterSlots extends Slots {
	readonly insertFooter?: Slot;
	readonly centerFooter?: Slot;
	readonly appendFooter?: Slot;
}
