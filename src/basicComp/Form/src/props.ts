import type { CSSProperties, PropType } from 'vue';
import type { ButtonProps } from 'ant-design-vue/es/button/buttonTypes';
import type { FieldMapToTime, FormActionType, FormProps, FormSchema } from './types/form';
import { ColEx } from './types/formItem';
import { RowProps } from 'ant-design-vue/lib/grid/Row';
import { TableActionType } from '#/Table';
import { propTypes } from '@/utils/propTypes';

export const basicFormItemProps = {
	schema: {
		type: Object as PropType<FormSchema>,
		default: () => {},
	},
	formProps: {
		type: Object as PropType<FormProps>,
		default: () => {},
	},
	allDefaultValues: {
		type: Object as PropType<Recordable>,
		default: () => ({}),
	},
	formModel: {
		type: Object as PropType<Recordable>,
		default: () => ({}),
	},
	setFormModel: {
		type: Function as PropType<(key: string, value: any, schema: FormSchema) => void>,
		default: null,
	},
	tableAction: {
		type: Object as PropType<TableActionType>,
	},
	formActionType: {
		type: Object as PropType<FormActionType>,
	},
};

type ButtonOptions = Partial<ButtonProps> & { text: string };
export const basicFormActionProps = {
	showActionButtonGroup: { type: Boolean, default: true },
	showResetButton: { type: Boolean, default: true },
	showSubmitButton: { type: Boolean, default: true },
	showAdvancedButton: { type: Boolean, default: false },
	resetButtonOptions: {
		type: Object as PropType<ButtonOptions>,
		default: () => ({}),
	},
	submitButtonOptions: {
		type: Object as PropType<ButtonOptions>,
		default: () => ({}),
	},
	actionColOptions: {
		type: Object as PropType<Partial<ColEx>>,
		default: () => ({}),
	},
	actionSpan: { type: Number, default: 6 },
	isAdvanced: { type: Boolean, default: false },
	hideAdvanceBtn: { type: Boolean, default: true },
};

export const basicFormProps = {
	model: {
		type: Object as PropType<Recordable>,
		default: {},
	},
	// 标签宽度  固定宽度
	labelWidth: {
		type: [Number, String] as PropType<number | string>,
		default: 0,
	},
	fieldMapToTime: {
		type: Array as PropType<FieldMapToTime>,
		default: () => [],
	},
	compact: propTypes.bool,
	// 表单配置规则
	schemas: {
		type: [Array] as PropType<FormSchema[]>,
		default: () => [],
	},
	mergeDynamicData: {
		type: Object as PropType<Recordable>,
		default: null,
	},
	baseRowStyle: {
		type: Object as PropType<CSSProperties>,
	},
	baseColProps: {
		type: Object as PropType<Partial<ColEx>>,
	},
	autoSetPlaceHolder: propTypes.bool.def(true),
	// 在INPUT组件上单击回车时，是否自动提交
	autoSubmitOnEnter: propTypes.bool.def(false),
	submitOnReset: propTypes.bool,
	submitOnChange: propTypes.bool,
	size: propTypes.oneOf(['default', 'small', 'large']).def('default'),
	// 禁用表单
	disabled: propTypes.bool,
	emptySpan: {
		type: [Number, Object] as PropType<number | Recordable>,
		default: 0,
	},
	// 是否显示收起展开按钮
	showAdvancedButton: propTypes.bool.def(false),
	// 转化时间
	transformDateFunc: {
		type: Function as PropType<Fn>,
		default: (date: any) => {
			return date?.format?.('YYYY-MM-DD HH:mm:ss') ?? date;
		},
	},
	rulesMessageJoinLabel: propTypes.bool.def(true),
	// 超过3行自动折叠
	autoAdvancedLine: propTypes.number.def(3),
	// 不受折叠影响的行数
	alwaysShowLines: propTypes.number.def(1),

	// 是否显示操作按钮
	showActionButtonGroup: propTypes.bool.def(true),
	// 操作列Col配置
	actionColOptions: Object as PropType<Partial<ColEx>>,
	// 显示重置按钮
	showResetButton: propTypes.bool.def(true),
	// 是否聚焦第一个输入框，只在第一个表单项为input的时候作用
	autoFocusFirstItem: propTypes.bool,
	// 重置按钮配置
	resetButtonOptions: Object as PropType<Partial<ButtonProps>>,

	// 显示确认按钮
	showSubmitButton: propTypes.bool.def(true),
	// 确认按钮配置
	submitButtonOptions: Object as PropType<Partial<ButtonProps>>,

	// 自定义重置函数
	resetFunc: Function as PropType<() => Promise<void>>,
	submitFunc: Function as PropType<() => Promise<void>>,

	// 以下为默认props
	hideRequiredMark: propTypes.bool,

	labelCol: Object as PropType<Partial<ColEx>>,

	layout: propTypes.oneOf(['horizontal', 'vertical', 'inline']).def('horizontal'),
	tableAction: {
		type: Object as PropType<TableActionType>,
	},

	wrapperCol: Object as PropType<Partial<ColEx>>,

	colon: propTypes.bool.def(true),

	labelAlign: propTypes.string,

	rowProps: Object as PropType<RowProps>,
};
