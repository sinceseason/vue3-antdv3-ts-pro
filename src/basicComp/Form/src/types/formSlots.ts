import { Slot, Slots } from 'vue';

export interface BasicFormSlots extends Slots {
	readonly formHeader?: Slot;
	readonly formFooter?: Slot;
}

export interface FormActions extends Slots {
	readonly resetBefore?: Slot;
	readonly submitBefore?: Slot;
	readonly advanceBefore?: Slot;
	readonly advanceAfter?: Slot;
}
