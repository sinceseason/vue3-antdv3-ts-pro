import { withInstall } from '@/utils';
import basicUpload from './src/BasicUpload.vue';

export type { PreviewFileItem, FileItem, FileChangeEventType } from './src/types/typing';

export const BasicUpload = withInstall(basicUpload);
export { getBase64WithFile, checkImgType } from './src/helper';
