import { withInstall } from '@/utils';

import appProvider from './src/AppProvider.vue';

export { useAppProviderContext } from './src/useAppCtx';

export const AppProvider = withInstall(appProvider);
