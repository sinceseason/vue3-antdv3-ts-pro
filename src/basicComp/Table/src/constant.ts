import componentSetting from '@/settings/componentSetting';

const { fetchFieldsSetting, defaultPageSize, pageSizeOptions, defaultFilterFn, defaultSorterFn } =
	componentSetting.table;

export const ROW_KEY = 'key';

export const ACTION_COLUMN_FLAG = 'ACTION';

export const DEFAULT_ALIGN = 'left';

export const FETCH_FIELD_SETTING = fetchFieldsSetting;

export const PAGE_SIZE = defaultPageSize;

export const PAGE_SIZE_OPTIONS = pageSizeOptions;

export const INDEX_COLUMN_FLAG = 'INDEX';

export const DEFAULT_FILTER_FN = defaultFilterFn;

export const DEFAULT_SORTER_FN = defaultSorterFn;
