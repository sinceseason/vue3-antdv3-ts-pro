import type { Ref } from 'vue';
import type { BasicTableProps } from '../types/table';
import type { TableActionType } from '../types/tableAction';
import { provide, inject, ComputedRef } from 'vue';

const key = Symbol('basic-table');

type Instance = TableActionType & {
	wrapperRef: Ref<Nullable<HTMLElement>>;
	getBindValues: ComputedRef<Recordable>;
};

type RetInstance = Omit<Instance, 'getBindValues'> & {
	getBindValues: ComputedRef<BasicTableProps>;
};

export function createTableContext(instance: Instance) {
	provide(key, instance);
}

export function useTableContext(): RetInstance {
	return inject(key) as RetInstance;
}
