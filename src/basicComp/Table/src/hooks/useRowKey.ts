import { computed, unref } from 'vue';
import type { ComputedRef } from 'vue';
import { BasicTableProps } from '../..';
import { ROW_KEY } from '../constant';

export function useRowKey(propsRef: ComputedRef<BasicTableProps>) {
	const getAutoCreateKey = computed(() => {
		return unref(propsRef).autoCreateKey! && !unref(propsRef).rowKey;
	});

	const getRowKey = computed(() => {
		const { rowKey } = unref(propsRef);
		return (unref(getAutoCreateKey) ? ROW_KEY : rowKey)!;
	});

	return {
		getAutoCreateKey,
		getRowKey,
	};
}
