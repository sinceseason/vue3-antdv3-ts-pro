import type { Ref, Slots } from 'vue';
import { h, computed, unref } from 'vue';
import { isString } from '@/utils/is';
import TableHeader from '../components/TableHeader.vue';
import { BasicTableProps } from '../types/table';
import { getSlot } from '@/utils/helper/tsxHelper';

export function useTableHeader(propsRef: Ref<BasicTableProps>, slots: Slots) {
	const getHeaderProps = computed(() => {
		const { title, titleHelpMessage, showTableSetting } = unref(propsRef);

		const hideTitle = !title && !slots.title && !showTableSetting;
		if (hideTitle && !isString(title)) {
			return {};
		}

		return {
			title: hideTitle
				? null
				: () =>
						h(
							TableHeader,
							{
								title,
								titleHelpMessage,
								showTableSetting,
							} as Recordable,
							{
								...(slots.toolbar
									? {
											toolbar: () => getSlot(slots, 'toolbar'),
									  }
									: {}),
								...(slots.title
									? {
											title: () => getSlot(slots, 'title'),
									  }
									: {}),
								...(slots.headerTop
									? {
											headerTop: () => getSlot(slots, 'headerTop'),
									  }
									: {}),
							},
						),
		};
	});

	return { getHeaderProps };
}
