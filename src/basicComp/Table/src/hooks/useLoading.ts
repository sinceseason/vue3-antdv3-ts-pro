import { ref, unref, computed, watch } from 'vue';
import type { Ref } from 'vue';
import { BasicTableProps } from '../types/table';

export function useLoading(propsRef: Ref<BasicTableProps>) {
	const loadingRef = ref(unref(propsRef).loading);
	// if(!loadingRef) return

	watch(
		() => unref(propsRef).loading,
		loading => {
			loadingRef.value = loading;
		},
	);

	const getLoading = computed(() => unref(loadingRef));

	function setLoading(loading: boolean) {
		loadingRef.value = loading;
	}

	return {
		getLoading,
		setLoading,
	};
}
