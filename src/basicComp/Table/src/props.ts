import { FormProps } from '#/Form';
import type { PropType } from 'vue';
import { DEFAULT_FILTER_FN, DEFAULT_SORTER_FN, FETCH_FIELD_SETTING } from './constant';
import { PaginationProps } from './types/pagination';
import { BasicColumn, FetchFieldSetting, TableSetting } from './types/table';
import { TableRowSelection } from './types/tableRowSelection';

export const basicProps = {
	// 表头上方的标题
	title: {
		type: [String, Function] as PropType<string | ((value: Recordable) => string)>,
	},
	titleHelpMessage: {
		type: [String, Array] as PropType<string | string[]>,
	},
	sorterFn: {
		type: Function as PropType<(data: Partial<Recordable<string[]>>) => any>,
		default: DEFAULT_SORTER_FN,
	},
	filterFn: {
		type: Function as PropType<(data: Partial<Recordable<string[]>>) => any>,
		default: DEFAULT_FILTER_FN,
	},
	columns: {
		type: Array as PropType<Array<BasicColumn>>,
		default: () => [],
	},
	// 是否显示序号
	showIndexColumn: {
		type: Boolean,
		default: false,
	},
	indexColumnProps: {
		type: Object as PropType<BasicColumn>,
		default: () => null,
	},
	actionColumn: {
		type: Object as PropType<BasicColumn>,
		default: null,
	},
	api: {
		type: Function as PropType<(...args: any[]) => Promise<any>>,
		default: null,
	},
	immediate: {
		type: Boolean,
		default: true,
	},
	fetchFieldsSetting: {
		type: Object as PropType<FetchFieldSetting>,
		default: () => FETCH_FIELD_SETTING,
	},
	// 额外的搜索条件
	searchInfo: {
		type: Object as PropType<Recordable>,
		default: null,
	},
	handleSearchInfoFn: {
		type: Function as PropType<Fn>,
		default: null,
	},
	// 额外的关键字信息
	keyWordInfo: {
		type: Object as PropType<Recordable>,
		default: null,
	},
	handleKeyWordInfoFn: {
		type: Function as PropType<Fn>,
		default: null,
	},
	// 是否使用loading
	loading: {
		type: Boolean,
		default: true,
	},
	rowKey: {
		type: [String, Function] as PropType<string | ((record: Recordable) => string)>,
		default: '',
	},
	dataSource: {
		type: Array as PropType<Recordable[]>,
		default: null,
	},
	// 是否为树形表格
	isTreeTable: {
		type: Boolean,
		default: false,
	},
	// 表格所有单元，超过长度是否省略，columns内部优先级更高
	ellipsis: {
		type: Boolean,
		default: false,
	},
	// 分页信息
	pagination: {
		type: [Object, Boolean] as PropType<boolean | PaginationProps>,
		default: null,
	},
	beforeFetch: {
		type: Function as PropType<Fn>,
		default: null,
	},
	afterFetch: {
		type: Function as PropType<Fn>,
		default: null,
	},
	// 显示表格设置
	tableSetting: {
		type: Object as PropType<TableSetting>,
		default: () => {},
	},
	showTableSetting: {
		type: Boolean,
		default: false,
	},
	// 使用搜索表单
	useSearchForm: {
		type: Boolean,
	},
	formConfig: {
		type: Object as PropType<Partial<FormProps>>,
		default: null,
	},
	// 点击表单重置按钮时是否重置表格的过滤和搜索
	resetWithTableSearchInfo: {
		type: Boolean as PropType<boolean>,
		default: false,
	},
	canResize: {
		type: Boolean,
		default: true,
	},
	maxHeight: {
		type: Number as PropType<number>,
	},
	isCanResizeParent: {
		type: Boolean,
		default: false,
	},
	resizeHeightOffset: {
		type: Number as PropType<number>,
		default: 0,
	},
	rowSelection: {
		type: Object as PropType<TableRowSelection<any> | null>,
		default: null,
	},
	autoCreateKey: { type: Boolean, default: true },
	clearSelectOnPageChange: { type: Boolean },
	striped: { type: Boolean, default: false },
	scroll: {
		type: Object as PropType<{ x: number | true; y: number }>,
		default: null,
	},
};
