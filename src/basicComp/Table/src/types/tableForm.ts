export interface TableForm {
	resetWithTableSearchInfo: boolean;
	filteredInfo: Recordable;
}
