import { Slot, Slots } from 'vue';

export interface TableTitleSlots extends Slots {
	readonly headerTop?: Slot;
	readonly toolbar?: Slot;
	readonly title?: Slot;
}

export interface TableSlots extends Slots {
	// before form and table
	readonly beforeTable?: Slot;
	// 展开多余行，与scroll互斥
	readonly expandedRowRender?: Slot;
}
