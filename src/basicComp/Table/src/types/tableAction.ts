import type { ButtonProps } from 'ant-design-vue';
import { GetColumnsParams } from './column';
import { PaginationProps } from './pagination';
import { BasicColumn, BasicTableProps, FetchParams } from './table';
import { TableRowSelection } from './tableRowSelection';

export type SizeType = 'default' | 'middle' | 'small' | 'large';

export interface TableActionType {
	reload: (opt?: FetchParams) => Promise<void>;
	setSelectedRows: (rows: Recordable[]) => void;
	getSelectRows: <T = Recordable>() => T[];
	clearSelectedRowKeys: () => void;
	expandAll: () => void;
	expandRows: (keys: string[]) => void;
	collapseAll: () => void;
	scrollTo: (pos: string) => void; // pos: id | "top" | "bottom"
	getSelectRowKeys: () => string[];
	deleteSelectRowByKey: (key: string) => void;
	setPagination: (info: Partial<PaginationProps>) => void;
	setDataSource: (values: Recordable[]) => void;
	updateTableDataRecord: (rowKey: string | number, record: Recordable) => Recordable | void;
	getColumns: (opt?: GetColumnsParams) => BasicColumn[];
	getDataSource: <T = Recordable>() => T[];
	getRawDataSource: <T = Recordable>() => T;
	setLoading: (loading: boolean) => void;
	setProps: (props: Partial<BasicTableProps>) => void;
	redoHeight: () => void;
	setSelectedRowKeys: (rowKeys: string[] | number[]) => void;
	getPaginationRef: () => PaginationProps | boolean;
	getRowSelection: () => TableRowSelection<Recordable>;
	emit?: EmitType;
	updateTableData: (index: number, key: string, value: any) => Recordable;
	setShowPagination: (show: boolean) => void;
	getShowPagination: () => boolean;
	setCacheColumnsByField?: (dataIndex: string | undefined, value: BasicColumn) => void;
}

export interface PopConfirm {
	title?: string;
	confirm: Fn;
	cancel?: Fn;
	okText?: string;
	cancelText?: string;
	icon?: string;
	placement?:
		| 'top'
		| 'left'
		| 'right'
		| 'bottom'
		| 'topLeft'
		| 'topRight'
		| 'bottomLeft'
		| 'bottomRight'
		| 'leftTop'
		| 'leftBottom'
		| 'rightTop'
		| 'rightBottom';
}

export interface ActionItem extends ButtonProps {
	label?: string;
	icon?: string;
	onClick?: Fn;
	color?: string;
	title?: string;
	href?: string;
	// 分隔线
	divider?: boolean;
	// 是否展示
	ifShow?: boolean | ((...args: any[]) => boolean);
	// TODO:权限控制
	auth?: string | string[];
	popConfirm?: PopConfirm;
	disabled?: boolean;
	btnProps?: ButtonProps;
}
