import { withInstall } from '@/utils';
import basicTable from './src/BasicTable.vue';
import tableAction from './src/components/TableAction.vue';
import tableTitle from './src/components/TableTitle.vue';
import tableHeader from './src/components/TableHeader.vue';

export * from './src/types/table';
export * from './src/types/tableAction';
export * from './src/types/pagination';
export { useTable } from './src/hooks/useTable';

export { default as TableImg } from './src/components/TableImg.vue';
export { default as EditTableHeaderIcon } from './src/components/EditTableHeaderIcon.vue';

export type { EditRecordRow } from './src/components/editable';

export const BasicTable = withInstall(basicTable);
export const TableAction = withInstall(tableAction);
export const TableTitle = withInstall(tableTitle);
export const TableHeader = withInstall(tableHeader);
