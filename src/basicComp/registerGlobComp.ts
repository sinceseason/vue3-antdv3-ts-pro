import type { App } from 'vue';
import { Button } from './Button';
import { Input, Layout } from 'ant-design-vue';
import * as components from './components';

const install = (app: App) => {
	Object.keys(components).forEach(key => {
		const comp = (components as any)[key];
		if (comp.install) {
			app.use(comp);
		}
	});
};

export function registerGlobComp(app: App) {
	app.use(Input).use(Button).use(Layout);
	app.use(install);
}
