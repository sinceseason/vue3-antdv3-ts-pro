/**
 * @description: menu type
 */
export enum MenuTypeEnum {
	// left menu
	SIDEBAR = 'sidebar',
	// mixin menu
	MIX = 'mix',
}

// 折叠触发器位置
export enum TriggerEnum {
	// 不显示
	NONE = 'NONE',
	// 菜单底部
	FOOTER = 'FOOTER',
	// 头部
	HEADER = 'HEADER',
}

// menu mode
export enum MenuModeEnum {
	INLINE = 'inline',
}
