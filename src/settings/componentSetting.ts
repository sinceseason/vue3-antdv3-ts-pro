import type { SorterResult } from '@/basicComp/Table';
import { isEmpty } from '@/utils/is';

export default {
	table: {
		// TODO: 支持自定义设置
		// 根据业务修改。如传参：{keyWordParam: {}, page: {[pageField]: 1, [pageSizeField]: 10}}。后台返回：{list: any[], totalCount: 70}
		fetchFieldsSetting: {
			pageField: 'pageStart',
			listField: 'list',
			pageSizeField: 'pageSize',
			totalField: 'totalCount',
			// 是否适配上海燃气的后台，若是，则传参为{keyWordParam: {}, page: {[pageField]: 1, [pageSizeField]: 10}}
			isShrq: false,
		},
		pageSizeOptions: ['10', '20', '50'],
		defaultPageSize: 10,
		// Custom general filter function
		defaultFilterFn: (data: Partial<Recordable<string[]>>) => {
			if (isEmpty(data)) {
				return data;
			}
			let rs = {} as Recordable;
			for (const key in data) {
				if (Object.prototype.hasOwnProperty.call(data, key)) {
					const val = data[key];
					rs[key] = val?.join(',');
				}
			}
			return rs;
		},
		// 自定义筛选方法，根据需求修改
		defaultSorterFn: (sortInfo: SorterResult) => {
			const { field, order, column } = sortInfo;
			if (column?.sorter === true && field && order) {
				return {
					field,
					order,
				};
			} else {
				return {};
			}
		},
	},
	scrollbar: {
		// 使用原生滚动条？
		// After opening, the menu, modal, drawer will change the pop-up scroll bar to native
		native: false,
	},
};
