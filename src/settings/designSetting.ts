import { ThemeEnum } from '../enums/appEnum';

export const prefixCls = 'zhoups';

export const darkMode = ThemeEnum.LIGHT;

// app theme preset color
export const APP_PRESET_COLOR_LIST: string[] = ['#0960bd', '#0084f4'];

// header preset color 数组类型为之后可选择颜色预留
export const HEADER_PRESET_BG_COLOR_LIST: string[] = ['#ffffff', '#001529'];

// sider preset color
export const SIDE_BAR_BG_COLOR_LIST: string[] = ['#001529', '#ffffff'];
