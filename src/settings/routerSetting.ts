// ROUTE_MAPPING模式下，和后台匹配路由的字段
export const mappingField = {
	// 父子对应关系，子[parentId] == 父[id]
	id: 'menuId',
	parentId: 'parentId',
	// 后台的路由匹配的唯一标识，前端固定为name，后台自定义如router
	name: 'name',
};
