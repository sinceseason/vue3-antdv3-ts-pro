import {
	ContentEnum,
	PermissionModeEnum,
	RouterTransitionEnum,
	SessionTimeoutProcessingEnum,
	SettingButtonPositionEnum,
	ThemeEnum,
} from '@/enums/appEnum';
import { CacheTypeEnum } from '@/enums/cacheEnum';
import { MenuModeEnum, MenuTypeEnum, TriggerEnum } from '@/enums/menuEnum';
import { ProjectConfig } from '~/config';
import { primaryColor } from '../../build/config/themeConfig';
import { HEADER_PRESET_BG_COLOR_LIST, SIDE_BAR_BG_COLOR_LIST } from './designSetting';

const setting: ProjectConfig = {
	// 是否展示设置按钮
	showSettingButton: false,

	settingButtonPosition: SettingButtonPositionEnum.AUTO,

	permissionMode: PermissionModeEnum.BACK,

	// Permission-related cache is stored in sessionStorage or localStorage
	permissionCacheType: CacheTypeEnum.LOCAL,

	// Session timeout processing
	sessionTimeoutProcessing: SessionTimeoutProcessingEnum.ROUTE_JUMP,

	// 主题颜色
	themeColor: primaryColor,

	// Whether to cancel the menu, the top, the multi-tab page display, for possible embedded in other systems
	fullContent: false,

	// content width
	contentMode: ContentEnum.FULL,

	// Whether to display the logo
	showLogo: true,
	// 在 type=mix 的模式下，logo的宽度是否和Menu一样宽？
	logoAsWideAsMenu: true,

	// Whether to show footer
	showFooter: false,

	headerSetting: {
		bgColor: HEADER_PRESET_BG_COLOR_LIST[1],
		// Whether to show top
		show: true,
		fixed: true,
		// theme
		theme: ThemeEnum.DARK,
		// 显示锁屏按钮
		useLockPage: true,
		showNotice: true,
		showSearch: false,
	},

	menuSetting: {
		// 侧边栏颜色
		bgColor: SIDE_BAR_BG_COLOR_LIST[1],
		// Menu type
		type: MenuTypeEnum.MIX,
		//  Whether to fix the left menu
		fixed: true,
		// Menu collapse
		collapsed: false,
		// 菜单收起时是否显示图标
		collapsedShowTitle: false,
		// 能否拖拽
		canDrag: true,
		// Menu width
		menuWidth: 210,
		// Menu mode
		mode: MenuModeEnum.INLINE,
		// 菜单主题
		theme: ThemeEnum.DARK,
		// 收起触发器位置
		trigger: TriggerEnum.FOOTER,
		// 菜單手風琴模式
		accordion: true,
	},

	// Multi-label
	multiTabsSetting: {
		// 刷新页面时是否缓存tabs
		cache: false,
		// 是否显示tabs
		show: true,
		// 是否拖动
		canDrag: true,
		// Turn on quick actions
		showQuick: true,
		// 是否展示刷新按钮
		showRedo: true,
		// Whether to show the collapse button
		showFold: true,
	},

	// Transition Setting
	transitionSetting: {
		// 当路由改变时是否显示动画
		// The disabled state will also disable pageLoadinng
		enable: true,

		// 路由的默认动画
		basicTransition: RouterTransitionEnum.FADE_SIDE,

		// Whether to open page switching loading
		// Only open when enable=true
		openPageLoading: true,

		// Whether to open the top progress bar
		openNProgress: true,
	},
	// Whether to enable KeepAlive cache is best to close during development, otherwise the cache needs to be cleared every time
	openKeepAlive: true,

	// Automatic screen lock time, 0 does not lock the screen. Unit minute default 0
	lockTime: 0,

	// Whether to show breadcrumbs
	showBreadCrumb: true,

	// Whether to show the breadcrumb icon
	showBreadCrumbIcon: false,
	// 显示回到顶部
	useOpenBackTop: false,
	closeMessageOnSwitch: true,
	canEmbedIFramePage: true,
	// Whether to cancel the http request that has been sent but not responded when switching the interface.
	removeAllHttpPending: false,
};
export default setting;
