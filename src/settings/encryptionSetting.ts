import { isDevMode } from '@/utils/env';

// storage过期时间, seconds
export const DEFAULT_CACHE_TIME = null;

export const cacheCipher = {
	key: 'RVwr+xXjP/yZOyee',
	iv: '@11111000001111_',
};

export const loginCipher = {
	key: '6TvaGVwq/Rb5',
};

// 是否加密
export const enableStorageEncryption = !isDevMode();
