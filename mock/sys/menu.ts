import { resultSuccess, resultError, getRequestToken, requestParams } from '../_util';
import { MockMethod } from 'vite-plugin-mock';
import { createFakeUserList } from './user';
import { cloneDeep } from 'lodash-es';

// single
const dashboardRoute = {
	path: '/dashboard',
	name: 'Dashboard',
	component: 'LAYOUT',
	redirect: '/dashboard/analysis',
	meta: {
		title: 'routes.dashboard.dashboard',
		hideChildrenInMenu: true,
		icon: 'bx:bx-home',
	},
	children: [
		{
			path: 'analysis',
			name: 'Analysis',
			component: '/dashboard/analysis/index',
			meta: {
				hideMenu: true,
				hideBreadcrumb: true,
				title: 'routes.dashboard.analysis',
				currentActiveMenu: '/dashboard',
				icon: 'bx:bx-home',
			},
		},
		{
			path: 'workbench',
			name: 'Workbench',
			component: '/dashboard/workbench/index',
			meta: {
				hideMenu: true,
				hideBreadcrumb: true,
				title: 'routes.dashboard.workbench',
				currentActiveMenu: '/dashboard',
				icon: 'bx:bx-home',
			},
		},
	],
};

const backRoute = {
	path: 'back',
	name: 'PermissionBackDemo',
	meta: {
		title: 'routes.demo.permission.back',
	},

	children: [
		{
			path: 'page',
			name: 'BackAuthPage',
			component: '/demo/permission/back/index',
			meta: {
				title: 'routes.demo.permission.backPage',
			},
		},
		{
			path: 'btn',
			name: 'BackAuthBtn',
			component: '/demo/permission/back/Btn',
			meta: {
				title: 'routes.demo.permission.backBtn',
			},
		},
	],
};

const authRoute = {
	path: '/permission',
	name: 'Permission',
	component: 'LAYOUT',
	redirect: '/permission/front/page',
	meta: {
		icon: 'carbon:user-role',
		title: 'routes.demo.permission.permission',
	},
	children: [backRoute],
};

const levelRoute = {
	path: '/level',
	name: 'Level',
	component: 'LAYOUT',
	redirect: '/level/menu1/menu1-1',
	meta: {
		icon: 'carbon:user-role',
		title: 'routes.demo.level.level',
	},

	children: [
		{
			path: 'menu1',
			name: 'Menu1Demo',
			meta: {
				title: 'Menu1',
			},
			children: [
				{
					path: 'menu1-1',
					name: 'Menu11Demo',
					meta: {
						title: 'Menu1-1',
					},
					children: [
						{
							path: 'menu1-1-1',
							name: 'Menu111Demo',
							component: '/demo/level/Menu111',
							meta: {
								title: 'Menu111',
							},
						},
					],
				},
				{
					path: 'menu1-2',
					name: 'Menu12Demo',
					component: '/demo/level/Menu12',
					meta: {
						title: 'Menu1-2',
					},
				},
			],
		},
		{
			path: 'menu2',
			name: 'Menu2Demo',
			component: '/demo/level/Menu2',
			meta: {
				title: 'Menu2',
			},
		},
	],
};

const sysRoute = {
	path: '/system',
	name: 'System',
	component: 'LAYOUT',
	redirect: '/system/account',
	meta: {
		icon: 'ion:settings-outline',
		title: 'routes.demo.system.moduleName',
	},
	children: [
		{
			path: 'account',
			name: 'AccountManagement',
			meta: {
				title: 'routes.demo.system.account',
				ignoreKeepAlive: true,
			},
			component: '/demo/sys/account/index',
		},
		{
			path: 'account_detail/:id',
			name: 'AccountDetail',
			meta: {
				hideMenu: true,
				title: 'routes.demo.system.account_detail',
				ignoreKeepAlive: true,
				showMenu: false,
				currentActiveMenu: '/system/account',
			},
			component: '/demo/sys/account/AccountDetail',
		},
		{
			path: 'role',
			name: 'RoleManagement',
			meta: {
				title: 'routes.demo.system.role',
				ignoreKeepAlive: true,
			},
			component: '/demo/sys/role/index',
		},

		{
			path: 'menu',
			name: 'MenuManagement',
			meta: {
				title: 'routes.demo.system.menu',
				ignoreKeepAlive: true,
			},
			component: '/demo/sys/menu/index',
		},
		{
			path: 'dept',
			name: 'DeptManagement',
			meta: {
				title: 'routes.demo.system.dept',
				ignoreKeepAlive: true,
			},
			component: '/demo/sys/dept/index',
		},
		{
			path: 'changePassword',
			name: 'ChangePassword',
			meta: {
				title: 'routes.demo.system.password',
				ignoreKeepAlive: true,
			},
			component: '/demo/sys/password/index',
		},
	],
};

const linkRoute = {
	path: '/link',
	name: 'Link',
	component: 'LAYOUT',
	meta: {
		icon: 'ion:tv-outline',
		title: 'routes.demo.iframe.frame',
	},
	children: [
		{
			path: 'doc',
			name: 'Doc',
			meta: {
				title: 'routes.demo.iframe.doc',
				frameSrc: 'https://vvbin.cn/doc-next/',
			},
		},
		{
			path: 'https://vvbin.cn/doc-next/',
			name: 'DocExternal',
			component: 'LAYOUT',
			meta: {
				title: 'routes.demo.iframe.docExternal',
			},
		},
	],
};

const nav = [
	// { name: 'Home', title: '首页', id: 1, parentId: 0 },
	// { name: 'Emergency', title: '应急基础信息管理', id: 9, parentId: 0 },
	// { name: 'EmergencyPerson', title: '应急人员管理', id: 10, parentId: 9 },
	// { name: 'EmergencyMaterial', title: '应急物资管理', id: 11, parentId: 9 },
	{ name: 'Level', title: '多级菜单', id: 16, parentId: 0 },
	{ name: 'Menu1Demo', title: 'Menu1', id: 17, parentId: 16 },
	{ name: 'Menu2Demo', title: 'Menu2', id: 18, parentId: 16 },
	{ name: 'Menu11Demo', title: 'Menu1-1', id: 19, parentId: 17 },
	{ name: 'Menu12Demo', title: 'Menu1-2', id: 20, parentId: 17 },
	{ name: 'Menu111Demo', title: 'Menu111', id: 21, parentId: 19 },
];

export default [
	{
		url: '/v3-basic-api/getMenuList',
		timeout: 1000,
		method: 'get',
		response: (request: requestParams) => {
			const token = getRequestToken(request);
			if (!token) {
				return resultError('Invalid token!');
			}
			const checkUser = createFakeUserList().find(item => token.includes(item.token));
			if (!checkUser) {
				return resultError('Invalid user token!');
			}

			const id = checkUser.userId;
			let menu: Object[];
			switch (id) {
				case 198179301163008:
					menu = nav;
					break;
				case 383516963700738:
					let copy = cloneDeep(nav);
					copy.splice(0, 1);
					menu = copy;
					break;
				case 1:
					dashboardRoute.redirect = dashboardRoute.path + '/' + dashboardRoute.children[0].path;
					menu = [dashboardRoute, authRoute, levelRoute, sysRoute, linkRoute];
					break;
				case 2:
					dashboardRoute.redirect = dashboardRoute.path + '/' + dashboardRoute.children[1].path;
					menu = [dashboardRoute, authRoute, levelRoute, linkRoute];
					break;
				default:
					dashboardRoute.redirect = dashboardRoute.path + '/' + dashboardRoute.children[0].path;
					menu = [dashboardRoute, authRoute, levelRoute, sysRoute, linkRoute];
					break;
			}
			return resultSuccess(menu);
		},
	},
] as MockMethod[];
