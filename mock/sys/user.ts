import { MockMethod } from 'vite-plugin-mock';
import { resultError, resultSuccess, getRequestToken, requestParams } from '../_util';

export function createFakeUserList() {
	return [
		{
			name: 'super用户',
			roleCode: ['admin'],
			roleId: [198174158061568],
			roleName: ['超级管理员'],
			token: 'eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJlY2hpc2FuIiwic3ViIjoiYWRtaW4iLCJp',
			userId: 198179301163008,
			username: 'admin',
			password: 'admin',
			// homePath: '/home',
		},
		{
			name: '钟 林',
			roleCode: ['yz_user'],
			roleId: [198175197282304],
			roleName: ['业主'],
			userId: 383516963700738,
			token: 'eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJlY2hpc2FuIiwic3ViIjoiemhvbmdsaW4i',
			// homePath: '/report',
			username: 'zhonglin',
			password: '123',
		},
	];
}

const fakeCodeList: any = {
	'1': ['1000', '3000', '5000'],

	'2': ['2000', '4000', '6000'],
};
export default [
	// mock user login
	{
		url: '/v3-basic-api/login',
		timeout: 200,
		method: 'post',
		response: ({ body }) => {
			const { username, password } = body;
			const checkUser = createFakeUserList().find(
				item => item.username === username && password === item.password,
			);
			if (!checkUser) {
				return resultError('Incorrect account or password！');
			}
			const { userId, username: _username, token, name, roleCode, roleId, roleName } = checkUser;
			return resultSuccess({
				userId,
				username: _username,
				token,
				name,
				roleCode,
				roleId,
				roleName,
			});
		},
	},
	{
		url: '/v3-basic-api/getUserInfo',
		method: 'get',
		response: (request: requestParams) => {
			const token = getRequestToken(request);
			if (!token) return resultError('Invalid token');
			const checkUser = createFakeUserList().find(item => token.includes(item.token));
			if (!checkUser) {
				return resultError('The corresponding user information was not obtained!');
			}
			return resultSuccess(checkUser);
		},
	},
	{
		url: '/v3-basic-api/getPermCode',
		timeout: 200,
		method: 'get',
		response: (request: requestParams) => {
			const token = getRequestToken(request);
			if (!token) return resultError('Invalid token');
			const checkUser = createFakeUserList().find(item => item.token === token);
			if (!checkUser) {
				return resultError('Invalid token!');
			}
			const codeList = fakeCodeList[checkUser.userId];

			return resultSuccess(codeList);
		},
	},
	{
		url: '/v3-basic-api/logout',
		timeout: 200,
		method: 'get',
		response: (request: requestParams) => {
			const token = getRequestToken(request);
			if (!token) return resultError('Invalid token');
			const checkUser = createFakeUserList().find(item => item.token === token);
			if (!checkUser) {
				return resultError('Invalid token!');
			}
			return resultSuccess(undefined, { msg: 'Token has been destroyed' });
		},
	},
] as MockMethod[];
