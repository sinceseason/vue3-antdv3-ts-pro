import { MockMethod } from 'vite-plugin-mock';
import { Random } from 'mockjs';
import { resultPageSuccess, resultSuccess } from '../_util';

function getRandomPics(count = 10): string[] {
	const arr: string[] = [];
	for (let i = 0; i < count; i++) {
		arr.push(Random.image('800x600', Random.color(), Random.color(), Random.title()));
	}
	return arr;
}

const demoList = (() => {
	const result: any[] = [];
	for (let index = 0; index < 200; index++) {
		result.push({
			id: `${index}`,
			beginTime: '@datetime',
			endTime: '@datetime',
			address: '@city()',
			name: '@cname()',
			name1: '@cname()',
			name2: '@cname()',
			name3: '@cname()',
			name4: '@cname()',
			name5: '@cname()',
			name6: '@cname()',
			name7: '@cname()',
			name8: '@cname()',
			avatar: Random.image('400x400', Random.color(), Random.color(), Random.first()),
			imgArr: getRandomPics(Math.ceil(Math.random() * 3) + 1),
			imgs: getRandomPics(Math.ceil(Math.random() * 3) + 1),
			date: `@date('yyyy-MM-dd')`,
			time: `@time('HH:mm')`,
			'no|100000-10000000': 100000,
			'status|1': ['normal', 'enable', 'disable'],
		});
	}
	return result;
})();

const detailList = id => {
	const result: any[] = [];
	for (let index = 0; index < 200; index++) {
		result.push({
			id: `${index}`,
			queryId: id,
			address: '@city()',
			name: '@cname()',
		});
	}
	return result;
};

const treeTable = [
	{
		id: 249323751490560,
		pid: 0,
		jobName: 'LNG码头',
		jobCode: 'E19037.C.01',
		planStartDate: '2022-01-28',
		planFinishDate: '2022-06-29',
		planDuration: 153,
		realStartDate: '2022-06-01',
		realFinishDate: '2022-06-01',
		level: 0,
		createTime: '2021-06-15 13:57:15',
		planFinishPercent: '20',
		finishPercent: '19',
		type: 3,
		keyNumber: 0,
		milestoneNumber: 0,
		children: [
			{
				id: 249323751785472,
				pid: 249323751490560,
				jobName: '码头LNG收集池',
				jobCode: 'E19037.C.01.02',
				planStartDate: null,
				planFinishDate: null,
				planDuration: 0,
				realStartDate: null,
				realFinishDate: null,
				level: 1,
				createTime: '2021-06-15 13:57:15',
				planFinishPercent: '0',
				finishPercent: '0',
				type: 3,
				keyNumber: 0,
				milestoneNumber: 0,
				children: null,
				finishStatus: 2,
				moudleStrList: ['02T0102'],
			},
			{
				id: 249323751851008,
				pid: 249323751490560,
				jobName: '码头操作平台及上部设施',
				jobCode: 'E19037.C.01.01',
				planStartDate: '2022-01-28',
				planFinishDate: '2022-06-29',
				planDuration: 153,
				realStartDate: '2022-01-28',
				realFinishDate: '2022-06-29',
				level: 1,
				createTime: '2021-06-15 13:57:15',
				planFinishPercent: '80',
				finishPercent: '80',
				type: 3,
				keyNumber: 0,
				milestoneNumber: 0,
				children: null,
				finishStatus: 2,
				moudleStrList: [
					'4"-M-0101004-S1-CC',
					'38"-LM-0101009-S1-CC',
					'26"-M-0101006-S1-CC',
					'38"-LM-0009004-S1-CC',
					'26"-M-0009001-S1-CC',
					'26"-M-0009002-S1-CC',
					'10"-M-0009003-S1-CC',
				],
			},
		],
		finishStatus: 1,
		moudleStrList: ['10"-WF-0101002-C3E-N'],
	},
	{
		id: 249323751949312,
		pid: 0,
		jobName: 'LNG罐区',
		jobCode: 'E19037.C.02',
		planStartDate: '2020-04-02',
		planFinishDate: '2022-07-01',
		planDuration: 818,
		realStartDate: '2020-04-02',
		realFinishDate: null,
		level: 0,
		createTime: '2021-06-15 13:57:15',
		planFinishPercent: '10',
		finishPercent: '10',
		type: 3,
		keyNumber: 0,
		milestoneNumber: 0,
		children: [
			{
				id: 249323751982080,
				pid: 249323751949312,
				jobName: '桩基施工',
				jobCode: 'E19037.C.02.14',
				planStartDate: '2020-04-02',
				planFinishDate: '2020-06-30',
				planDuration: 87,
				realStartDate: '2020-04-02',
				realFinishDate: '2020-06-30',
				level: 1,
				createTime: '2021-06-15 13:57:15',
				planFinishPercent: '100',
				finishPercent: '100',
				type: 3,
				keyNumber: 1,
				milestoneNumber: 0,
				children: [
					{
						id: 249323752031232,
						pid: 249323751982080,
						jobName: 'LNG罐区桩基',
						jobCode: 'E19037.C.02.14.1',
						planStartDate: '2020-04-02',
						planFinishDate: '2020-06-30',
						planDuration: 87,
						realStartDate: '2020-04-02',
						realFinishDate: '2020-06-30',
						level: 2,
						createTime: '2021-06-15 13:57:15',
						planFinishPercent: '100',
						finishPercent: '100',
						type: 3,
						keyNumber: 0,
						milestoneNumber: 0,
						children: null,
						finishStatus: 2,
						moudleStrList: [],
					},
				],
				finishStatus: 2,
				moudleStrList: ['03E0601-01'],
			},
			{
				id: 249323752080384,
				pid: 249323751949312,
				jobName: '储罐承台施工',
				jobCode: 'E19037.C.02.15',
				planStartDate: '2020-06-16',
				planFinishDate: '2020-09-30',
				planDuration: 107,
				realStartDate: '2020-06-16',
				realFinishDate: '2020-09-30',
				level: 1,
				createTime: '2021-06-15 13:57:15',
				planFinishPercent: '100',
				finishPercent: '100',
				type: 3,
				keyNumber: 1,
				milestoneNumber: 0,
				children: null,
				finishStatus: 2,
				moudleStrList: [],
			},
		],
	},
];

export default [
	{
		url: '/v3-basic-api/table/getDemoList',
		timeout: 100,
		method: 'get',
		response: ({ query }) => {
			const { pageStart = 1, pageSize = 20 } = query;
			return resultPageSuccess(pageStart, pageSize, demoList);
		},
	},
	{
		url: '/v3-basic-api/table/getTreeTable',
		timeout: 100,
		method: 'get',
		response: () => {
			return resultSuccess(treeTable);
		},
	},
	{
		url: '/v3-basic-api/table/getDetailList',
		timeout: 100,
		method: 'get',
		response: ({ query }) => {
			const { pageStart = 1, pageSize = 5 } = query.page;
			return resultPageSuccess(pageStart, pageSize, detailList(query.id));
		},
	},
] as MockMethod[];
