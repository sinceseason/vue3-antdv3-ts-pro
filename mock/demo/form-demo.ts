import { MockMethod } from 'vite-plugin-mock';
// import { Random } from 'mockjs';
import { resultPageSuccess } from '../_util';

const whList = (page, size) => {
	const result: any[] = [];
	for (let index = (page - 1) * size; index < page * size - 1; index++) {
		result.push({
			id: `${index}`,
			address: '@city()',
			name: '@name()',
		});
	}
	return result;
};

export default [
	{
		url: '/v3-basic-api/wh/list',
		timeout: 100,
		method: 'get',
		response: ({ query }) => {
			const { pageStart = 1, pageSize = 10 } = query;
			return resultPageSuccess(pageStart, pageSize, whList(pageStart, pageSize));
		},
	},
] as MockMethod[];
