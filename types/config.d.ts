import { TriggerEnum, MenuModeEnum, MenuTypeEnum } from '@/enums/menuEnum';
import {
	ContentEnum,
	PermissionModeEnum,
	ThemeEnum,
	RouterTransitionEnum,
	SettingButtonPositionEnum,
	SessionTimeoutProcessingEnum,
} from '@/enums/appEnum';

import { CacheTypeEnum } from '@/enums/cacheEnum';

export interface MenuSetting {
	bgColor: string;
	fixed: boolean;
	collapsed: boolean;
	collapsedShowTitle: boolean;
	canDrag: boolean;
	menuWidth: number;
	theme: ThemeEnum;
	trigger: TriggerEnum;
	accordion: boolean;
	type: MenuTypeEnum;
	mode: MenuModeEnum;
}

export interface MultiTabsSetting {
	cache: boolean;
	show: boolean;
	showQuick: boolean;
	canDrag: boolean;
	showRedo: boolean;
	showFold: boolean;
}

export interface HeaderSetting {
	bgColor: string;
	show: boolean;
	fixed: boolean;
	theme: ThemeEnum;
	useLockPage: boolean;
	showNotice: boolean;
	showSearch: boolean;
}

export interface TransitionSetting {
	//  Whether to open the page switching animation
	enable: boolean;
	// Route basic switching animation
	basicTransition: RouterTransitionEnum;
	// Whether to open page switching loading
	openPageLoading: boolean;
	// Whether to open the top progress bar
	openNProgress: boolean;
}

export interface ProjectConfig {
	showSettingButton: boolean;
	settingButtonPosition: SettingButtonPositionEnum;
	permissionMode: PermissionModeEnum;
	permissionCacheType: CacheTypeEnum;
	sessionTimeoutProcessing: SessionTimeoutProcessingEnum;
	themeColor: string;
	// The main interface is displayed in full screen, the menu is not displayed, and the top
	fullContent: boolean;
	contentMode: ContentEnum;
	showLogo: boolean;
	logoAsWideAsMenu: boolean;
	showFooter: boolean;
	headerSetting: HeaderSetting;
	menuSetting: MenuSetting;
	multiTabsSetting: MultiTabsSetting;
	transitionSetting: TransitionSetting;
	openKeepAlive: boolean;
	lockTime: number;
	showBreadCrumb: boolean;
	showBreadCrumbIcon: boolean;
	useOpenBackTop: boolean;
	canEmbedIFramePage: boolean;
	closeMessageOnSwitch: true;
	removeAllHttpPending: boolean;
}

export interface GlobConfig {
	// Site title
	title: string;
	// Service interface url
	apiUrl: string;
	// Upload url
	uploadUrl?: string;
	//  Service interface url prefix
	urlPrefix?: string;
	// Project abbreviation
	shortName: string;
}
export interface GlobEnvConfig {
	// Site title
	VITE_GLOB_APP_TITLE: string;
	// Service interface url
	VITE_GLOB_API_URL: string;
	// Service interface url prefix
	VITE_GLOB_API_URL_PREFIX?: string;
	// Project abbreviation
	VITE_GLOB_APP_SHORT_NAME: string;
	// Upload url
	VITE_GLOB_UPLOAD_URL?: string;
}
