declare interface ViteEnv {
	VITE_USE_MOCK: boolean;
	VITE_PUBLIC_PATH: string;
	VITE_GLOB_APP_TITLE: string;
	VITE_GLOB_APP_SHORT_NAME: string;
	VITE_USE_CDN: boolean;
	VITE_DROP_CONSOLE: boolean;
	VITE_BUILD_COMPRESS: 'gzip' | 'brotli' | 'none';
	VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE: boolean;
	VITE_LEGACY: boolean;
	VITE_USE_IMAGEMIN: boolean;
}

declare interface ImportMetaEnv extends ViteEnv {
	__: unknown;
}
