export {};

declare module 'vue-router' {
	interface RouteMeta extends Record<string | number | symbol, unknown> {
		// 菜单排序，只对第一级有效
		orderNo?: number;
		// title
		title: string;
		// title重复时可取别名，用于tab栏的区分显示
		titleAlias?: string;
		// 动态路由tab.
		dynamicLevel?: number;
		// 动态路由实际path
		realPath?: string;
		// 是否忽略权限?可添加前端路由而不受后台控制。只对第一级生效
		ignoreAuth?: boolean;
		// 可访问的角色编码，可根据业务修改。只在权限模式为Role时生效
		roleCode?: string[];
		// 是否忽略keepalive缓存？
		ignoreKeepAlive?: boolean;
		// 是否在头部固定tab
		affix?: boolean;
		// icon on tab
		icon?: string;
		// 内嵌frame地址
		frameSrc?: string;
		// 路由切换动画
		transitionName?: string;
		// 隐藏该路由在面包屑中的显示？
		hideBreadcrumb?: boolean;
		// 隐藏所有子菜单
		hideChildrenInMenu?: boolean;
		// 如果该路由会携带参数，且需要在tab页上显示，设置为true
		carryParam?: boolean;
		// 是否为单级页面,若是则取子路由的第一个
		single?: boolean;
		// 当前激活的菜单。用于配置详情页或属于当前菜单且不展示的路由时指定左侧激活的菜单
		currentActiveMenu?: string;
		// 当前路由不在头部的标签页显示
		hideTab?: boolean;
		// 当前路由不在菜单内显示
		hideMenu?: boolean;
		// 是否为超链接,设置path为链接地址即可
		isLink?: boolean;
		// 在ROUTE_MAPPING以及BACK模式下，生成对应的菜单而忽略路由
		ignoreRoute?: boolean;
		// 为子项目生成菜单时忽略本级
		hidePathForChildren?: boolean;
	}
}
