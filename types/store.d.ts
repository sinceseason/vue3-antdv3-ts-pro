import { MenuModeEnum, MenuTypeEnum } from '@/enums/menuEnum';

// Lock screen information
export interface LockInfo {
	// Password required
	pwd?: string | undefined;
	// Is it locked?
	isLock?: boolean;
}

export interface BeforeMiniState {
	menuCollapsed?: boolean;
	menuSplit?: boolean;
	menuMode?: MenuModeEnum;
	menuType?: MenuTypeEnum;
}
