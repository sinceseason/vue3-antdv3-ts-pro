declare interface Fn<T = any> {
	(...args: T[]): T;
}

declare interface PromiseFn<T = any> {
	(...args: T[]): Promise<T>;
}

declare type RefType<T> = T | null;

declare type LabelValueOptions = {
	label: string;
	value: any;
	[key: string]: string | number | boolean;
}[];

declare type TargetContext = '_self' | '_blank';

declare type EmitType = (event: string, ...args: any[]) => void;

declare interface ComponentElRef<T extends HTMLElement = HTMLDivElement> {
	$el: T;
}

declare type ComponentRef<T extends HTMLElement = HTMLDivElement> = ComponentElRef<T> | null;

declare type ElRef<T extends HTMLElement = HTMLDivElement> = Nullable<T>;
