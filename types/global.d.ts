import type {
	ComponentRenderProxy,
	VNode,
	VNodeChild,
	ComponentPublicInstance,
	FunctionalComponent,
	PropType as VuePropType,
	readonly,
} from 'vue';

declare global {
	declare type PropType<T> = VuePropType<T>;
	declare type VueNode = VNodeChild | JSX.Element;

	declare type Writable<T> = {
		-readonly [P in keyof T]: T[P];
	};

	declare type Nullable<T> = T | null;
	declare type NonNullable<T> = T extends null | undefined ? never : T;
	declare type Undefable<T> = T | undefined;
	declare type Recordable<T = any> = Record<string, T>;
	declare type ReadonlyRecordable<T = any> = {
		readonly [key: string]: T;
	};

	declare type DeepPartial<T> = {
		[P in keyof T]?: DeepPartial<T[P]>;
	};

	declare type TimeoutHandle = ReturnType<typeof global.setTimeout>;
	declare type IntervalHandle = ReturnType<typeof global.setInterval>;

	declare interface ChangeEvent extends Event {
		target: HTMLInputElement;
	}

	namespace JSX {
		// tslint:disable no-empty-interface
		type Element = VNode;
		// tslint:disable no-empty-interface
		type ElementClass = ComponentRenderProxy;
		interface ElementAttributesProperty {
			$props: any;
		}
		interface IntrinsicElements {
			[elem: string]: any;
		}
		interface IntrinsicAttributes {
			[elem: string]: any;
		}
	}
}
