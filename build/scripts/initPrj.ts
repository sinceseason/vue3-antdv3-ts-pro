// #!/usr/bin/env node
import { writeFile } from 'fs';
import path from 'path';
import chalk from 'chalk';
import { cwd } from 'process';
import rimraf from 'rimraf';

const dirs = [
	'image',
	'mock/demo',
	'src/api/demo',
	'src/router/routes/modules/*.ts',
	'src/views/demo',
	'src/views/func',
];

dirs.map(item => {
	rimraf(item, err => {
		if (err) {
			console.error(chalk.red(`😒 删除/${item}失败:${err}`));
		}
		console.log(chalk.green(`🤣 删除/${item}成功！`));
	});
});

writeFile(
	path.join(cwd(), './src/router/routes/async-routes.ts'),
	`import { Demo, LAYOUT } from '../constant';
import { AppRouteRecordRaw } from '../types';

let asyncRoute: AppRouteRecordRaw[] = [
	{
		path: 'home',
		name: 'Home',
		component: () => import('@/views/home/Home.vue'),
		meta: {
			title: '首页',
			icon: 'bx:bx-home',
		},
	},
	{
		path: 'emergency',
		name: 'Emergency',
		component: LAYOUT,
		redirect: '/emergency/person',
		meta: {
			title: '应急基础信息管理',
			icon: 'bx:bx-home',
		},
		children: [
			{
				path: 'person',
				name: 'EmergencyPerson',
				component: Demo,
				meta: {
					title: '应急人员管理',
				},
			},
			{
				path: 'material',
				name: 'EmergencyMaterial',
				component: Demo,
				meta: {
					title: '应急物资管理',
				},
			},
		],
	},
];

export default asyncRoute;`,
	err => {
		if (err) {
			console.error(chalk.red(`😒 修改/src/router/routes/async-routes.ts文件失败:${err}`));
			process.exit(1);
		}
		console.log(chalk.green(`🤣 修改/src/router/routes/async-routes.ts文件成功!`));
	},
);
