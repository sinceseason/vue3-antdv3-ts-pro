import * as readline from 'readline';

const question = `please make sure you have change your project's version?' (yes/no): `;

const questionInterface = readline.createInterface({
	input: process.stdin,
	output: process.stderr,
});
questionInterface.question(question, answer => {
	questionInterface.close();

	const normalizedAnswer = answer.toLowerCase().startsWith('y');

	if (!normalizedAnswer) {
		console.error('You need to change it!');
		process.exitCode = 1;

		return;
	}
});
