import type { PluginOption } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import legacy from '@vitejs/plugin-legacy';
import windiCSS from 'vite-plugin-windicss';
import vitePluginZipDist from './zip';
import purgeIcons from 'vite-plugin-purge-icons';
import { configHtmlPlugin } from './html';
import { configThemePlugin } from './theme';
import { configMockPlugin } from './mock';
import { configImageminPlugin } from './imagemin';
import { configCompressPlugin } from './compress';
import pkg from '../../package.json';

export function createVitePlugins(viteEnv: ViteEnv, isBuild: boolean) {
	const {
		VITE_USE_MOCK,
		VITE_LEGACY,
		VITE_USE_IMAGEMIN,
		VITE_BUILD_COMPRESS,
		VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE,
	} = viteEnv;

	const vitePlugins: (PluginOption | PluginOption[])[] = [vue(), vueJsx()];

	vitePlugins.push(windiCSS());

	VITE_LEGACY && isBuild && vitePlugins.push(legacy());

	vitePlugins.push(configHtmlPlugin(viteEnv, isBuild));

	VITE_USE_MOCK && vitePlugins.push(configMockPlugin(isBuild));

	vitePlugins.push(configThemePlugin(isBuild));

	vitePlugins.push(purgeIcons());

	if (isBuild) {
		VITE_USE_IMAGEMIN && vitePlugins.push(configImageminPlugin());

		vitePlugins.push(configCompressPlugin(VITE_BUILD_COMPRESS, VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE));

		vitePlugins.push(
			vitePluginZipDist({
				zipName: pkg.name + '_' + pkg.version,
				dayjsFormat: 'YYYYMMDDHHmmss',
			}),
		);
	}

	return vitePlugins;
}
