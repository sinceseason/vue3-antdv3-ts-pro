/**
 * https://github.com/anncwb/vite-plugin-style-import
 */
import { createStyleImportPlugin, AndDesignVueResolve } from 'vite-plugin-style-import';

export function configStyleImportPlugin(isBuild: boolean) {
	if (!isBuild) {
		return [];
	}
	const styleImportPlugin = createStyleImportPlugin({
		resolves: [AndDesignVueResolve()],
		libs: [
			{
				libraryName: 'ant-design-vue',
				esModule: true,
				resolveStyle: name => {
					return `../../node_modules/ant-design-vue/es/${name}/style/index`;
				},
			},
		],
	});
	return styleImportPlugin;
}
